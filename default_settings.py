# -*- coding: utf-8 -*-
# Django settings for project.


import os
from os.path import dirname, abspath, join

SITE_NAME = "Hostile au Style"
DEBUG = False

#Custom:
SITE_ROOT = abspath(dirname(__file__))

ADMINS = (
    # ('Your Name', 'your_email@domain.com'),
)

MANAGERS = ADMINS

# Pour les mail automatisés (notifications de commentaires)
DEFAULT_FROM_EMAIL = "no-reply@hostile-au-style.fr"

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': SITE_ROOT + '/data.sqlite'
    }
}

# Do we have to force the name used for example in link generation ?
FORCE_SCRIPT_NAME= ""

# Local time zone for this installation. All choices can be found here:
# http://www.postgresql.org/docs/current/static/datetime-keywords.html#DATETIME-TIMEZONE-SET-TABLE
TIME_ZONE = 'Europe/Paris'

# Language code for this installation. All choices can be found here:
# http://www.w3.org/TR/REC-html40/struct/dirlang.html#langcodes
# http://blogs.law.harvard.edu/tech/stories/storyReader$15
LANGUAGE_CODE = 'fr-fr'
DEFAULT_CHARSET = "utf-8"

SITE_ID = 1
SITE_URL = 'http://localhost:8000/'

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = SITE_ROOT + '/site_media/'

# Accept 10Mio uploads (photos)
DATA_UPLOAD_MAX_MEMORY_SIZE = 10 * 1024 * 1024

# URL that handles the media served from MEDIA_ROOT.
# Example: "http://media.lawrence.com"
MEDIA_URL = '/site_media/'

STATIC_URL = '/static/'

STATICFILES_DIRS = [
    join(SITE_ROOT, "static"),
]

# Make this unique, and don't share it with anybody.
SECRET_KEY = ')t3j^l(!z^o$fgg8gm^j(0aw3@)9x^1!@@zsf6s1%b@0dlx*m('

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            SITE_ROOT + "/templates",
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "texte.context_processors.global_authors_info",
            ]
        },
    },
]

TEST_RUNNER = 'django.test.runner.DiscoverRunner'

MIDDLEWARE = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
#   'debug_toolbar.middleware.DebugToolbarMiddleware',
    #   'djprofiler.ProfileMiddleware',
]

ROOT_URLCONF = 'urls'


INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sites',
    'django.contrib.sessions',
    'django.contrib.staticfiles',
    'django.contrib.syndication',
    'django.contrib.messages',
    'django.contrib.admin',
    'markdownx',
    'tagging',
    'captcha',
    'captchaproxy',
    'texte',
    'bugreport',
    'articles',
    'subscriptions',
#    'debug_toolbar'
)


AUTH_PROFILE_MODULE = "texte.ProfilAuteur"

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'django': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
        },
    }
}

## settings

# Lorsque un utilisateur ne publie pas de photo.
DEFAULT_PHOTO='portraits/default.png'

DEFAULT_USER=2
DEFAULT_LICENCE=2


PDF_ROOT = '%s/pdf' % MEDIA_ROOT
PDF_URL = '%s/pdf' % MEDIA_URL

BANNERS = (STATIC_URL + '/img/banniere1.png',
           STATIC_URL + '/img/banniere2.png',
           STATIC_URL + '/img/banniere3.png',
           STATIC_URL + '/img/banniere4.png',
           STATIC_URL + '/img/banniere5.png')

INTERNAL_IPS = ('127.0.0.1',)

CAPTCHA_FONT_SIZE=22
CAPTCHA_NOISE_FUNCTIONS=()

JWT_OPTIONS = {
   'verify_signature': True,
   'verify_exp': True,
   'verify_nbf': True,
   'verify_iat': True,
   'verify_aud': True,
   'require_exp': True,
   'require_iat': False,
   'require_nbf': False
}

# in seconds, from the time it's created
JWT_DEFAULT_LIFETIME = 3600
