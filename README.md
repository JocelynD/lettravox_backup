LettraVox
=========

LettraVox is a litterature web publishing tool, it aims to offer to writters a
space to publish texts from different styles (poetry, books, novelas...)
online.

It takes care about typography and readability, each text is available in two
formats (online html and offline PDF). User feedback is available to comment the
text or to report errors.

Install
--------

Install debian packages:

    sudo apt-get install libjpeg-dev python3-venv texlive-latex-extra \
        texlive-lang-french texlive-humanities latex2html python3-dev \
        libpng-dev libfreetype6-dev

Install archlinux packages:

    sudo pacman -S openjpeg python3 python3-virtualenv texlive-latexextra \
        texlive-langextra texlive-humanities latex2html \
        libpng freetype2 sqlite

Create a virtualenv

    python3 -m venv venv
    source venv/bin/activate

Install python requirements:

    pip install -U setuptools
    pip install -r requirements.txt

Then copy local_settings

    cp local_settings.py{.example,}

And edit *local_settings.py* to set a `SECRET_KEY`. For this, you can
use the following command:

    python -c 'import random; import string; print("".join([random.SystemRandom().choice(string.digits + string.letters) for i in range(100)]))'

Bootstrap the database

    ./manage.py migrate

You must be able to start a test server now (don't forget to add DEBUG =
True in your *local_settings.py*):

    ./manage.py runserver

Please note THIS IS A TEST SERVER. Do NOT use it in
production. Lettravox is in fact a Django application. Please take time
to read Django documentation to know how to properly serves Lettravox on
a production server.

You may now want to add your first user (admin). You must kill the test
server (CTRL+C) and then enter the following command:

    ./manage.py createsuperuser

You are done

Developping
--------------

To run tests :

    pip install -r dev-requirements.txt
    py.test
