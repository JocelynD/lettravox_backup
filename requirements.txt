django-tagging>=0.5.0,<0.6
Pillow>=7.2.0,<8
Django>=2.2,<2.3
html2text==2015.6.21
django-simple-captcha>=0.5.11,<0.6
django-markdownx>=2.0.21,<2.1
