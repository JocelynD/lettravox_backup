from django.template import Library
from ..forms import EmailForm, RSSForm

register = Library()

@register.inclusion_tag('subscriptions/tags/abo_forms.html')
def abo_forms(pre_checked=''):
    """
    :param pre_checked: "+"-separated field names initially checked
    """
    pre_checked_list = {i:True for i in pre_checked.split('+') if len(i) > 0}
    return {
        'email_form' : EmailForm(initial=pre_checked_list),
        'rss_form' : RSSForm(initial=pre_checked_list)
    }
