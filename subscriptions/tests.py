# -*- coding: utf-8 -*-

import re
import datetime

from django.contrib.auth.models import User
from django.core import mail
from django.test import TestCase
from freezegun import freeze_time

from texte.models import Texte
from articles.models import Article
from .models import EmailSubscription
from .views import UnsubscriptionSigner

class EmailSubscriptionProcess(TestCase):
    re_link = re.compile(r'http://.*/valider/[\w\d\._:%-]+/')

    def test_double_optin_ok(self):
        # require subscription
        response = self.client.post('/abonnements/email/',{
            'email': 'f@example.com'
        }, follow=True)
        self.assertEqual(len(response.redirect_chain), 1)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(mail.outbox), 1)
        link = self.re_link.search(mail.outbox[0].body).group(0)

        # browse the link
        response = self.client.get(link)
        self.assertEqual(response.status_code, 200)

        # get registered !
        sub = EmailSubscription.objects.get(email='f@example.com')
        self.assertEqual(sub.targets, 'texts')

    def test_confirm_link_idempotence(self):
        self.client.post('/abonnements/email/',{
            'email': 'f@example.com'
        })
        link = self.re_link.search(mail.outbox[0].body).group(0)
        resp1 = self.client.get(link)
        resp2 = self.client.get(link)
        self.assertEqual((resp1.status_code, resp2.status_code), (200, 200))
        self.assertEqual(EmailSubscription.objects.count(), 1)

    def test_confirm_different_inscription_updates(self):
        self.client.post('/abonnements/email/',{
            'email': 'f@example.com'
        })
        self.client.post('/abonnements/email/',{
            'email': 'f@example.com'
        })

        link1 = self.re_link.search(mail.outbox[0].body).group(0)
        self.client.get(link1)
        link2 = self.re_link.search(mail.outbox[1].body).group(0)
        self.client.get(link2)

        self.assertEqual(EmailSubscription.objects.count(), 1)
        self.assertEqual(EmailSubscription.objects.all()[0].targets,
                         'texts')

    def test_unsubscription_ok(self):
        sub = EmailSubscription.objects.create(email='sausage@example.com',
                                               targets='texts')
        url = '/abonnements/email/desinscription/{}/'.format(
            UnsubscriptionSigner.sign(1))

        # step 1
        response = self.client.get(url)
        assert response.status_code == 200
        assert EmailSubscription.objects.count() == 1

        # step 2 & 3
        response = self.client.post(url, follow=True)
        assert response.status_code, 200
        assert len(response.redirect_chain) ==  1
        assert EmailSubscription.objects.count() == 0


class SubscriptionTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='SomeWriter')

    def create_text(self,
                    titre='foo é',
                    auteur=None,
                    est_compile=True,
                    est_publie=True,
                    resume='If I resume...'):
        if not auteur:
            auteur = self.user

        return Texte.objects.create(
            titre=titre,
            auteur=auteur,
            est_compile=est_compile,
            est_publie=est_publie,
            resume=resume
        )


class EmailNotificationsTests(SubscriptionTestCase):
    def test_notify_published_text(self):
        EmailSubscription.objects.create(email='sausage@example.com',
                                         targets='texts')
        self.create_text()
        self.assertEqual(len(mail.outbox), 1)
        self.assertIn('/textes/foo-e/', mail.outbox[0].body)
        self.assertIn('/desinscription/', mail.outbox[0].body)

    def test_dont_notify_unpublished_text(self):
        EmailSubscription.objects.create(email='sausage@example.com',
                                         targets='texts')
        with freeze_time('2013-05-02'):
            self.create_text(titre='lili')
        with freeze_time('2101-05-02'):
            self.create_text(titre='lélé')

        self.create_text(est_compile=False, titre="lala")
        self.create_text(est_publie=False, titre="lolo")

        assert len(mail.outbox) == 0

    def test_dont_notify_modified_text(self):
        EmailSubscription.objects.create(email='sausage@example.com',
                                         targets='texts')
        text = self.create_text()
        mail.outbox = []
        text.titre = 'bar'
        text.save()
        assert len(mail.outbox) == 0

    def test_notify_on_text_publication(self):
        EmailSubscription.objects.create(email='sausage@example.com',
                                         targets='texts')

        text = self.create_text(est_compile=False)
        text.est_compile = True
        text.save()
        assert len(mail.outbox) == 1

    def test_notify_published_event(self):
        EmailSubscription.objects.create(email='bacon@example.com',
                                         targets='events')
        EmailSubscription.objects.create(email='sausage@example.com',
                                         targets='texts+paperbacks')

        mail.outbox = []
        Article.objects.create(
            auteur=self.user,
            categorie=Article.EVENEMENT,
            texte='<p>On fait un concerté, youhouuuuu</p>',
            titre='Concert lu et chanté au chateau de Vezelay (42)'
        )
        self.assertEqual(len(mail.outbox), 1)
        self.assertIn('/actu/concert-lu-et-chante-au-chateau-de-vezelay-42/',
                      mail.outbox[0].body)
        self.assertIn('/desinscription/', mail.outbox[0].body)

    def test_dont_notify_unpublished_event(self):
        EmailSubscription.objects.create(email='bacon@example.com',
                                         targets='events')
        EmailSubscription.objects.create(email='sausage@example.com',
                                         targets='texts+paperbacks')

        mail.outbox = []
        Article.objects.create(
            auteur=self.user,
            categorie=Article.EVENEMENT,
            publication_status=Article.PUB_STATE_DRAFT,
            texte='<p>On fait un concerté, youhouuuuu</p>',
            titre='Concert lu et chanté au chateau de Vezelay (42)'
        )
        self.assertEqual(len(mail.outbox), 0)

    def test_dont_notify_future_event(self):
        EmailSubscription.objects.create(email='bacon@example.com',
                                         targets='events')
        mail.outbox = []
        Article.objects.create(
            auteur=self.user,
            categorie=Article.EVENEMENT,
            publication_status=Article.PUB_STATE_PUBLISHED,
            pub_date=datetime.datetime(2048, 2, 2),
            texte='<p>On fait un concerté, youhouuuuu</p>',
            titre='Concert lu et chanté au chateau de Vezelay (42)'
        )
        self.assertEqual(len(mail.outbox), 0)

    def test_notify_published_paperback(self):
        EmailSubscription.objects.create(email='sausage@example.com',
                                         targets='paperbacks+texts')
        EmailSubscription.objects.create(email='bacon@example.com',
                                         targets='texts')
        EmailSubscription.objects.create(email='bacon@example.com',
                                         targets='events')
        mail.outbox = []
        Article.objects.create(
            auteur=self.user,
            categorie=Article.PUBLICATION,
            texte='<p>On sort des livres, youhouuuuué</p>',
            titre='Nouvel écrit : le poil dans la main'
        )
        self.assertEqual(len(mail.outbox), 1)
        self.assertIn('/actu/nouvel-ecrit-le-poil-dans-la-main/',
                      mail.outbox[0].body)
        self.assertEqual(len(mail.outbox), 1)
        self.assertIn('/desinscription/', mail.outbox[0].body)

    def test_not_notify_edited_event(self):
        EmailSubscription.objects.create(
            email='bacon@example.com', targets='events')
        art = Article.objects.create(
            auteur=self.user,
            categorie=Article.EVENEMENT,
            texte='<p>On fait un concerté, youhouuuuu</p>',
            titre='Concert lu et chanté au chateau de Vezelay (42)'
        )
        mail.outbox = []
        art.titre = 'Concert chaloupé'
        art.save()

        self.assertEqual(len(mail.outbox), 0)

    def test_no_escape_on_text_mails(self):
        EmailSubscription.objects.create(email='sausage@example.com',
                                         targets='texts')
        texte = self.create_text(
            est_compile=False,
            titre="x'y",  # would be escaped by Django autoescape
        )
        texte.est_compile = True
        texte.save()
        self.assertIn("x'y", mail.outbox[0].body)
        print(mail.outbox[0].body)


class TestViews(SubscriptionTestCase):
    def test_full_rss(self):
        self.create_text(titre='IYIY')
        response = self.client.get('/abonnements/rss/texts+events+paperbacks/')
        assert response.status_code == 200
        assert 'IYIY' in response.content.decode()
