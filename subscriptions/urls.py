from django.conf.urls import url

from .views import *
from .feeds import MultiRSS

urlpatterns = [
    url(r'^rss/$', rss_sub_process, name='rss-sub-process'),
    url(r'^rss/(?P<keywords>[-\w+]+)/$', MultiRSS(), name='multi-rss'),
    url(r'^email/valider/(?P<token>[\w\d\._:-]+)/$', email_sub_validate, name='email-sub-validate'),
    url(r'^email/desinscription/(?P<token>[\w\d\._:-]+)/$', email_sub_unsubscribe, name='email-sub-unsubscribe'),
    url(r'^email/$', email_sub_process, name='email-sub-process'),
]
