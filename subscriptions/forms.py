# -*- coding: utf-8 -*-

from django import forms

class SubscriptionForm(forms.Form):
    texts = forms.BooleanField(label='Textes en ligne', required=False)
    events = forms.BooleanField(label='Événements', required=False)
    paperbacks = forms.BooleanField(label='Parutions papier', required=False)

    def enabled_options(self):
        return [k for k,v in self.data.items()
                if k in ('texts', 'events', 'paperbacks')]

class RSSForm(SubscriptionForm):
    pass

class EmailForm(forms.Form):
    email = forms.EmailField(widget=forms.TextInput(
        attrs={'type': 'email', 'required': 'yes',
               'placeholder': 'votre addresse mail'}))
