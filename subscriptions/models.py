# -*- coding: utf-8 -*-

from urllib.parse import urljoin

from django.db import models
from django.conf import settings
from django.core.validators import RegexValidator
from django.urls import reverse

from utils.mail import send_service_mail
from .signing import UnsubscriptionSigner

class EmailSubscriptionManager(models.Manager):
    def subscribed_to(self, target):
        return super(EmailSubscriptionManager, self).get_queryset().filter(
            targets__contains=target)


class EmailSubscription(models.Model):
    email = models.EmailField()
    targets = models.CharField(max_length=50,
                               validators=[RegexValidator(r'^\w+(\+\w+)*$')],
                               blank=True)
    date = models.DateTimeField(auto_now_add=True)

    objects = EmailSubscriptionManager()


    def __str__(self):
        return self.email

    def notify(self):
        raise NotImplementedError()

    def get_unsubscription_link(self):
        return urljoin(
            settings.SITE_URL,
            reverse('email-sub-unsubscribe',
                    kwargs={'token': UnsubscriptionSigner.sign(self.pk)}))

# FIXME: Remove with dj 1.7, using manager-queryset conversion
class TextEmailSubscriptionManager(EmailSubscriptionManager):
    def get_queryset(self):
        return super(TextEmailSubscriptionManager, self).subscribed_to('texts')


class TextEmailSubscription(EmailSubscription):
    objects = TextEmailSubscriptionManager()

    class Meta:
        proxy = True

    def notify(self, obj):
        send_service_mail(
            "Nouveau texte : {}".format(obj.titre),
            self.email,
            "subscriptions/mails/notification_new_text.txt", {
                'text': obj,
                'unsubscribe_url': self.get_unsubscription_link()
            })


class EventEmailSubscriptionManager(EmailSubscriptionManager):
    def get_queryset(self):
        return super(EventEmailSubscriptionManager, self).subscribed_to('events')

class EventEmailSubscription(EmailSubscription):
    objects = EventEmailSubscriptionManager()

    class Meta:
        proxy = True

    def notify(self, obj):
        send_service_mail("Nouvel événement : {}".format(obj.titre),
                          self.email,
                          "subscriptions/mails/notification_new_article.txt", {
                              'article': obj,
                              'unsubscribe_url': self.get_unsubscription_link()
                          })

class PaperbackEmailSubscriptionManager(EmailSubscriptionManager):
    def get_queryset(self):
        return super(PaperbackEmailSubscriptionManager, self).subscribed_to('paperbacks')


class PaperbackEmailSubscription(EmailSubscription):
    objects = PaperbackEmailSubscriptionManager()

    class Meta:
        proxy = True

    def notify(self, obj):
        send_service_mail("Nouvelle publication papier : {}".format(obj.titre),
                          self.email,
                          "subscriptions/mails/notification_new_article.txt", {
                              'article': obj,
                              'unsubscribe_url': self.get_unsubscription_link()
                          })

# FIXME: use dj-1.7 apps notion to remove that
from . import signals
