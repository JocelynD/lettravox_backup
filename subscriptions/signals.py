import datetime

from django.db.models.signals import post_save
from django.dispatch import receiver

from texte.models import Texte
from articles.models import Article
from .models import (TextEmailSubscription, EventEmailSubscription,
                     PaperbackEmailSubscription)


@receiver(post_save, sender=Texte, dispatch_uid='notify-email-new-text')
def notify_email_new_text(sender, instance, created, raw, *args, **kwargs):
    text = instance
    if (not raw
        and (text.pub_date.date() == datetime.date.today())
            and text.just_published):
        for sub in TextEmailSubscription.objects.all():
            sub.notify(text)


@receiver(post_save, sender=Article, dispatch_uid='notify-email-new-article')
def notify_email_new_article(sender, instance, created, raw, *args, **kwargs):
    article = instance
    if (
            not raw
            and
            created
            and
            article.pub_date <= datetime.datetime.now()
            and
            article.publication_status == article.PUB_STATE_PUBLISHED
    ):
        if article.is_publication():
            subscribers = PaperbackEmailSubscription.objects.all()
        else:
            subscribers = EventEmailSubscription.objects.all()
        for sub in subscribers:
            sub.notify(article)
