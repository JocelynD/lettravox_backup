# -*- coding: utf-8 -*-

from base64 import urlsafe_b64encode, urlsafe_b64decode

from django.conf import settings
from django.urls import reverse
from django.http import HttpResponseBadRequest, HttpResponse
from django.shortcuts import redirect, render, get_object_or_404

from .forms import EmailForm
from .models import EmailSubscription
from .signing import SubscriptionSigner, UnsubscriptionSigner, BadSignature
from utils.mail import send_service_mail


def rss_sub_process(request):
    keywords = '+'.join(list(request.GET.keys()))
    view = reverse('multi-rss', kwargs={'keywords': keywords})
    return redirect(view, permanent=True)

def email_sub_process(request):
    """ Initiate double opt-in email registration
    """
    if request.POST:
        form = EmailForm(request.POST)
        if not form.is_valid():
            return HttpResponseBadRequest()

        to_addr = form.data['email']
        form = EmailForm(request.POST)
        token = SubscriptionSigner.sign({
            'type': 'email-subscription',
            'addr': to_addr,
            'subscriptions': ['texts'],
        })
        validation_url = request.build_absolute_uri(
            reverse('email-sub-validate', kwargs={'token': token}))
        send_service_mail(
            "Confirmation d'inscription aux notifications",
            to_addr,
            "subscriptions/mails/subscription_confirm.txt",
            {'validation_url': validation_url}
        )
        return redirect('.')
    else:
        return render(request, 'subscriptions/email_sub_waiting_validation.html')

def email_sub_validate(request, token):
    """Finishes double opt-in email registration
    """
    try:
        data = SubscriptionSigner.unsign(token)
        subscription_str = '+'.join(data['subscriptions'])
        obj, created = EmailSubscription.objects.get_or_create(
            email=data['addr'],
            defaults={'targets': subscription_str})
        if not created and (obj.targets != subscription_str):
            obj.targets = subscription_str
            obj.save()
        return render(request, 'subscriptions/email_validated.html',
                                  {'email': data['addr']})
    except BadSignature:
        return HttpResponseBadRequest(
            'Jeton invalide ou expiré, renouvellez votre inscription :-)')


def email_sub_unsubscribe(request, token):
    is_GET = (request.method == 'GET')
    is_POST = (request.method == 'POST')
    try:
        pk = UnsubscriptionSigner.unsign(token)
    except BadSignature:
        return HttpResponseBadRequest(
            'Jeton invalide ou expiré, renouvellez votre inscription :-)')
    if is_GET and 'ok' in request.GET:  # step 3
        return render(request, 'subscriptions/email_unsub_ok.html',
                                  {'email': request.GET.get('addr')})
    else:
        sub = get_object_or_404(EmailSubscription, pk=pk)
        if is_GET:  # step 1
            return render(request, 'subscriptions/email_unsub_confirm.html',
                                      {'email': sub.email})
        elif is_POST:  # step 2
            sub.delete()
            return redirect('?addr={}&ok'.format(sub.email))
