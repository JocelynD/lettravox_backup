# -*- coding: utf-8 -*-

from itertools import chain

from django.conf import settings
from django.contrib.syndication.views import Feed
from django.utils.encoding import force_text

from articles.feeds import EvenementsRSS, PublicationsRSS
from texte.feeds import TextesRSS

def natural_enum_list(l):
    """
    >>> natural_enum([1, 2, 3])
    '1, 2 et 3'
    """
    s = ''
    for i, x in enumerate(reversed(l)):
        if i > 1:
            yield ','
        elif i > 0:
            yield 'et'
        yield x


def natural_enum(l):
    """
    :param l: iterable

    >>> enumerate(['a', 'b', 'c'])
    'a, b et c'
    """
    return ' '.join(
            reversed(
                list(
                natural_enum_list(l)
    ))).replace(' ,', ',')


class MultiRSS(Feed):
    SLAVE_FEEDS = {
        'texts': TextesRSS,
        'events': EvenementsRSS,
        'paperbacks': PublicationsRSS
    }
    link = '/'

    def get_object(self, *args, **kwargs):
        keywords = kwargs['keywords']
        words = keywords.split('+')
        l = []
        for i in words:
            try:
                l.append(self.SLAVE_FEEDS[i])
            except KeyError:
                pass
        return l

    def title(self, obj):
        return '{} - {}'.format(
            settings.SITE_NAME,
            natural_enum(
                list(map(force_text, (i.short_title for i in obj)
                ))).lower().capitalize())

    def description(self, obj):
        return self.title(obj)

    def categories(self, obj):
        return [].extend(chain(obj))

    def items(self, obj):
        return chain.from_iterable(
            ((i(), j) for j in list(i().items()))
            for i in obj)

    def item_description(self, item):
        obj, item = item
        return obj.item_description(item)

    def item_title(self, item):
            obj, item = item
            return '{} − {}'.format(obj.prefix, obj.item_title(item))

    def item_link(self, item):
        obj, item = item
        return obj.item_link(item)
