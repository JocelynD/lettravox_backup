# -*- coding: utf-8 -*-


from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='EmailSubscription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(max_length=75)),
                ('targets', models.CharField(blank=True, max_length=50, validators=[django.core.validators.RegexValidator('^\\w+(\\+\\w+)*$')])),
                ('date', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EventEmailSubscription',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('subscriptions.emailsubscription',),
        ),
        migrations.CreateModel(
            name='PaperbackEmailSubscription',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('subscriptions.emailsubscription',),
        ),
        migrations.CreateModel(
            name='TextEmailSubscription',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('subscriptions.emailsubscription',),
        ),
    ]
