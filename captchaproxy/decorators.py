from django.shortcuts import render
from captchaproxy.forms import CaptchaForm

import time

def captcha_protected(func):
    def do_captcha(request, *args, **kwargs):
        if request.GET or request.user.is_authenticated \
                or request.session.get('is_human'):
            return func(request, *args, **kwargs)
        else:
            # "captcha_1" and "captcha_0 are IDs from django-simple-captcha forms
            if "captcha_1" in request.POST:
                if CaptchaForm(request.POST).is_valid():
                    request.session['is_human'] = True
                    return func(request, *args, **kwargs)
                else:
                    # to avoid putting these fields in user_form
                    orig_POST = request.POST.copy()
                    del orig_POST['captcha_1']
                    del orig_POST['captcha_0']
                    request.POST = orig_POST

            form = CaptchaForm()
            return render(request, "captchaproxy/captcha_form.html", {"form":form, "user_form":request.POST})
    return do_captcha
