��          T      �       �   �   �      �     �     �  ?   �       5  (  2  ^  	   �  '   �     �  E   �  %   "                                        As many automated scripts spams our website throught comments, we use this captcha system to detect bots. You are only asked on the first comment you post on the website, you then keep considerated as a human for some time. Confirm Confirm Your humanity Prove me you're human : Type the letters you see on the left picture into the text box. Why is this page needed? Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2009-10-01 01:19+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Depuis que des robots polluent notre site de publicité (<em>spam</em>) via les commentaires, nous utilisons ce système de captcha pour détecter ces robots. Ce test ne vous est soumis que la première fois que vous postez sur le site, vous êtes ensuite considéré comme humain pour une certaine durée. Confirmer Confirmation de votre caractère humain Prouve-moi ton humanité Recopiez les lettres apparaissant à gauche dans la boîte de droite. Pourquoi cette page est nécessaire ? 