# -*- coding: utf-8 -*-

from django.contrib import admin
import django.contrib.auth.admin
from django.contrib.auth.models import User
from django.contrib.admin.widgets import AdminFileWidget
from django.utils.safestring import mark_safe

from texte.models import (
    AuteurExterne,
    CategorieReferenceExterne,
    Commentaire,
    Genre,
    Illustration,
    Lien,
    Licence,
    Modele,
    ProfilAuteur,
    ReferenceExterne,
    Section,
    Texte,
)
from texte.templatetags import lvtags


# Widget that displays a miniature on ImageFields
# http://www.psychicorigami.com/2009/06/20/django-simple-admin-imagefield-thumbnail/
class AdminImageWidget(AdminFileWidget):
    thumb_size = "700x400"
    id = "illustration"

    def render(self, name, value, attrs=None, renderer=None):
        output = []
        if value and getattr(value, "url", None):
            image_url = value.url
            thumb_url = lvtags.thumbnail(image_url, self.thumb_size)
            file_name = str(value)
            output.append('<input type="hidden" id="full_%s_width" value="%i"/><img id="%s" src="%s" alt="%s" /><br />' % \
                (self.id, value.width, self.id, thumb_url, file_name))
        output.append(super(AdminFileWidget, self).render(name, value, attrs, renderer))
        return mark_safe(''.join(output))


class AdminImageSquareWidget(AdminImageWidget):
    thumb_size = "100x100"
    id = "square"


class ProfilAuteur_Inline(admin.StackedInline):
    model = ProfilAuteur
    max_num = 1
    extra = 1
    can_delete = False
    fieldsets = [
        ('Profil', {'fields': [
            'is_author',
            'photo',
            'anniv',
            'influences',
            'hobies',
        ]}),
        ('Contact par mail', {'fields': [
            'aut_email',
            'aut_formulaire',
        ]}),
        ('site perso', {'fields': [
            'website_url',
            'website_text',
        ]}),
        ("newsletter de l'auteure", {'fields': [
            'newsletter_url',
            'newsletter_text',
        ]}),
        ('ailleurs sur le web', {'fields': [
            'url_don',
        ]}),
    ]

    class Media:
        css = {
            "all": ['style/custom_admin/profilauteur_inline.css']
        }


class ProfilAuteurAdmin(django.contrib.auth.admin.UserAdmin):
    inlines = [ProfilAuteur_Inline]

    def get_fieldsets(self, *args, **kwargs):
        # Collapse « dates importantes » and « permissions »
        ret = super(ProfilAuteurAdmin, self).get_fieldsets(*args, **kwargs)
        rw_ret = [
            [fieldset[0], fieldset[1]]
            for fieldset in ret
        ]
        rw_ret[2][1]['classes'] = ('collapse',)
        rw_ret[3][1]['classes'] = ('collapse',)
        return rw_ret


class ReferenceExterne_Inline(admin.StackedInline):
    model = ReferenceExterne
    num = 1
    extra = 1


class TexteAdmin(admin.ModelAdmin):
    class Media:
        js = ("js/prototype.js", "js/foreignImage.js")

    list_display = (
        'titre',
        'pub_date',
        'auteur',
        'nombre_com',
        'bouton_compiler',
    )
    # FIXME: Come back to custom form
    fieldsets = (
        (None, {'fields': (
            'titre',
            'pub_date',
            'est_publie',
        )}),
        ('Contenu', {'fields': (
            'fichier_source',
            'resume',
        )}),
        ('Métadonnées', {'fields': (
            'illustration',
            'licence',
            'genre',
            'tags',
        )}),
    )
    list_filter = ['auteur', 'pub_date', 'genre', 'est_publie', 'est_compile']
    search_fields = ['titre', 'resume']
    date_hierarchy = 'pub_date'
    inlines = [ReferenceExterne_Inline]

    # Ces deux méthodes sont redéfinies afin de permettre le choix de l'auteur
    # automatique.

    def save_model(self, request, obj, form, change):
        # Si l'objet est nouveau, on définit son auteur
        # automatiquement.
        if not hasattr(obj, 'auteur'):
            obj.auteur = request.user
        obj.save()


class CommentaireAdmin(admin.ModelAdmin):
    list_display = ('auteur', 'pub_date')
    list_filter = ['auteur', 'pub_date']
    search_fields = ['auteur', 'texte']
    date_hierarchy = 'pub_date'


class IllustrationAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': (
            'fichier',
            'square',
            'square_size',
            'square_x_pos',
            'square_y_pos',
            'auteur',
        )}),
    ]

    class Media:
        css = {"all": ("style/admin-crop.css",)}
        js = ("js/prototype.js", "js/crop.js")

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name in ('fichier', 'square'):
            if db_field.name == 'fichier':
                kwargs['widget'] = AdminImageWidget
            else:  # square
                kwargs['widget'] = AdminImageSquareWidget
            try:
                del kwargs['request']
            except KeyError:
                pass
            return db_field.formfield(**kwargs)
        return super(IllustrationAdmin, self).formfield_for_dbfield(
            db_field, **kwargs
        )


admin.site.register(Genre)
admin.site.register(AuteurExterne)
admin.site.register(Texte, TexteAdmin)
admin.site.register(Commentaire, CommentaireAdmin)
admin.site.register(Section)
admin.site.register(Lien)
admin.site.register(Licence)
admin.site.register(Modele)
admin.site.register(CategorieReferenceExterne)
admin.site.register(ReferenceExterne)
admin.site.register(Illustration, IllustrationAdmin)
admin.site.unregister(User)
admin.site.register(User, ProfilAuteurAdmin)
