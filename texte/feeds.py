# -*- coding: utf-8 -*-
from django.contrib.syndication.views import Feed
from django.conf import settings

from texte.models import Texte

class TextesRSS(Feed):
    title = "%s - Derniers textes" % settings.SITE_NAME
    short_title = 'Derniers textes'
    prefix = 'Texte'
    link = "/textes/"
    description = "Derniers textes publiés sur %s" % settings.SITE_NAME
    categories = ['texte en ligne']

    def items(self):
        return Texte.objects.published().order_by('-pub_date')[:10]

    def item_title(self, item):
        return item.titre

    def item_description(self, item):
        return item.resume

    def author_name(self, item):
        if item:
            return item.auteur
