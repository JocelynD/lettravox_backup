class DocumentAbstraction(object):
  """ Squelette, inutilisable """

  def __init__.py:
      pass
  self.editor=True # Si un éditeur en ligne est disponible
  self.log = False # Contient un journal d'erreur 
  
  def compile(self, source, force = True):
    """
    source : Chemin vers le fichier source
    force : Force la compilation, même si des erreurs non-fatales apparaissent.
    """
    pass

  def menage(self):
    """
    Retire les fichiers temporaires
    """
    pass

  def get_pdf(self):
    pass

  def get_html(self):
    pass



