#! /usr/bin/env python
# -*-  coding: utf-8 -*-
import os
import re
import tempfile
import subprocess

from django.conf import settings

def dev_null():
    return open('/dev/null', 'r+')

def enter(dir):
    """ Change le repertoire courant vers dir.
    Dir peut obtenir certaines valeurs spéciales :
      * "tmp" changera le répertoire courant vers un répertoire
        temporaire fraîchement créé ;
      * "medias" changera le répertoire courant vers le dossier de
         medias de Django;
      * "medias_tex" fera de même mais à l'intérieur du sous-dossier
    tex_src.

    Cette fonction renvoie une liste de trace utilisé par la focntion
    leave pour revenir au répertoire précédent.
    trace : (ancien_répertoire, temporaire), temporaire étant un
    booléen indiquant si le répertoire qui  vient d'être pénetré est un
    réperoire temporaire.
    """

    path = {'tmp':tempfile.mkdtemp(),
            'medias':settings.MEDIA_ROOT,
            'medias_tex':os.path.join(settings.MEDIA_ROOT,'tex_src'),
            'medias_pdf':os.path.join(settings.PDF_ROOT)
           }

    old_path = os.path.abspath('.')
    os.chdir(path[dir])

    is_tmp = (dir == 'tmp')

    return (old_path,path[dir],is_tmp) # Trace

def leave(trace):
    """ Leave récupère un dictionnaire de trace de la focntion enter()
    et revient au répertoire précédent. Si le répertoire quitté est
    un répertoire temporaire, leave() effacera recursivement tout son
    contenu.
    """
    os.chdir(trace[0])
    if trace[2]:
        #BEWARE, it removes recursively the tmp dir
        for root, dirs, files in os.walk(trace[1],topdown=False):
            for i in files:
                os.remove(os.path.join(root,i))
            os.rmdir(root)


def epurate(string):
    """
    Remplace toutes les successions d'espaces et de retours à la ligne
    de string par un simple espace blanc pour chaque. Les espaces de
    début et de fin sont également effacés
    """
    import re
    spaces = re.compile(r'\s{2,}')
    string = spaces.sub(' ',string)
    return string.strip()

def tidy(filepath):
    """ Utilise l'utilitaire tidy pour obtenir un code xhtml
    corectemment indenté... Modifie le fichier désigné par filepath
    lui-même. """
    x = subprocess.Popen("tidy  -indent -bare -asxhtml -m -utf8 %s"%filepath,
                         shell=True, stderr=dev_null())
    x.wait()


def latex2html(texpath):
    """ Lance l'utilitaire latex2html sur le fichier situé au chemin
    indiqué par texpath
    """
    x = subprocess.Popen("latex2html -html_version 4.0,unicode -split 4 -no_navigation -no_subdir -no_footnode -info 0 -lcase_tags -link 0 %s"%texpath,
                         shell=True, stderr=dev_null(), stdout=dev_null())

    return x.wait()

def strip(soap):
    """
    Vire d'un fichier html produit par latex2html tout ce qui est
    inutile à savoir le head et le pied de page. le résultat retourné
    est destiné à être inclus dans une page xhtml pré-existante.
    """
    keep_body = re.compile(r"<body\s*>(.*)<hr ?/?>\s*</body>",re.DOTALL | re.MULTILINE)
    snippet = keep_body.search(soap).group(1)
    return snippet

def tex2inner(texcode):
    """ Prend du code LaTteX en argument, le code sera compilé en html puis
    manipulé ,pour aboutir  à un objet de sortie de type
    sortie = {"index":((1, Titre de la première partie),(3,titre de la
                      deuxième partie)),
              "pages":(,None,"contenu html de la page 1","contenu html de la page 2")
             }
    """

    short = "tohtml.tex"
    toreturn = {"pages":[],"index":[]}
    html_files = []
    enter_trace =  enter('tmp')
    cur_dir = enter_trace[0]

    open(short,'w').write(texcode)

    latex2html(short)
    try:
        os.remove("tohtml.html") # A strange extra-file...
    except OSError:
        pass  # No longer present on recent (debian stretch) latex2html

    # On boucle sur tous les fichiers html génerés dans le répertoire temporaire.
    for i in os.listdir('.'):
        if i.endswith(".html"):
            html_files.append(i)

    #print html_files

    nb_pages = len(html_files)
    single = (nb_pages == 1)

    reg = re.compile(r'(index)|((node)(\d+)).html')

    # prépare une liste de la bonne taile

    for i in range(nb_pages + single):
        toreturn["pages"].append(None)

    for i in html_files:
        tidy(i)
        inner = strip(open(i,'r').read())

        # Dans le groupe
        regsearch = reg.search(i)
        if regsearch:
            content = inner
            if regsearch.group(1) and not single: # index
                # toreturn['index'] = content
                # Cherchons la structure
                # groupes de regexp: (0): n°page ; (1): titre
                chaine_re = r'<li><a name=\s*"tex2html\d+" href=\s*"node(\d+)\.html" id=\s*"tex2html\d+">([^</]*)</a>'
                rstruct = re.compile(chaine_re,re.DOTALL)
                for part in rstruct.findall(content):
                    # latex2html fait la conversion '' --> '.' On la refait à l'inverse.
                    if part[1] == '.':
                        titre = ''
                    else:
                        titre = part[1]
                    toreturn['index'].append((part[0],epurate(titre)))

            elif regsearch.group(2) or single: #node = page
                content_propre = epurate(content)
                content_propre = clean_page(content_propre, single)
                #print "OK COMPUTER"
                if single:
                    no_page = 1
                else:
                    no_page = int(regsearch.group(4))
                toreturn["pages"][no_page] = content_propre
        else:
            raise TypeError

    toreturn["single"] = single
    leave(enter_trace)
    return toreturn


def add_nbsp(texte):
    """ Empêche les signes de ponctuation d'être décolés des mots.
    ex: un point d'exclamation seul à la ligne.
    """
    nbsp_bef = re.compile(r'\s+([!?»:;])')
    nbsp_aft = re.compile(r'(«)\s+')

    texte = nbsp_bef.sub(lambda m: "&nbsp;%s" % m.group(1), texte)
    texte = nbsp_bef.sub(lambda m: "%s&nbsp;" % m.group(1), texte)

    return texte

def clean_page(texte,single):
    """Nettoie La page de texte qui sera inclus """

    # Enlève les id et les name des titres
    #vire_id = re.compile(r'<a \s*((id|name)=\s*"SECTION\d{20}"\s*){2}\s*>(.*)</a>')
    #texte = vire_id.sub(lambda match: match.group(3),texte)


    # Met une majuscule à tous les titres
    cap_titre = re.compile(r'(<h[1-6]>)(\w)')
    texte = cap_titre.sub(lambda match: "%s%s"%(match.group(1),match.group(2).capitalize()), texte)
    texte = texte.replace("<p>",'<p class="matiere">')

    #print "STEP1"
    #print texte


    #Rempalce le hn par un hn+2
    h1toh3 = re.compile(r'<h([1-6])(\s*\w+="\w*")*?>(.*?)</h[1-6]>',re.DOTALL)
    texte =  h1toh3.sub(lambda m: "<h%i>%s</h%i>"%(
        int(m.group(1)) + 2,m.group(3),int(m.group(1)) + 2),texte)
    #print "STEP 1.5"
    #print texte

    if single:
        #<h3 align="center">Aupres de toi</h3> on l'enlève
        # Il est inutile de garder un titre de chapitre si il n'y a qu'une partie...
        rmh3 = re.compile(r'<h3>.*</h3>')
        texte =  rmh3.sub(lambda match: "",texte)

    #print "STEP2"
    #print texte

    # Vide les titres que latex2html a rempli d'un point et leur affecte la classe "vide"
    rm_points = re.compile(r'<h([1-6]).*?>\.<.*?/h[1-6]>',re.DOTALL)
    if rm_points.search(texte):
        texte =  rm_points.sub(lambda m: '<h%s class="vide"></h%s>'%(m.group(1),m.group(1)),texte)


    #print "STEP3"
    #print texte


    # Rend les références relatives
    # href= "node1.html#tex2html2"
    absreb = re.compile(r'href=\s*"\w+\.html(#tex2html\d)"')
    if absreb.search(texte):
        texte =  absreb.sub(lambda m: 'href="%s"'%m.group(1),texte)

    texte = add_nbsp(texte)

    #print "STEP6"
    #print texte

    return texte
