# -*- coding: utf-8 -*-
from . import utils
import subprocess
import re
import os
from texte.Documents import exceptions

class Abstraction:
    """ Fournit une abstraction pour les fichiers LaTex. Nécessite
    PDFLatex pour la compilation en pdf et Latex2html pour la
    conversion en html. La méthode compile() returne True si tout va
    bien. Sinon, une exception est levée.FeatureError si le format
    n'est pas supporté, LatexError si une erreur intervient pendant la
    compilation Latex.

    * Le nom du fichier pdf est stockée dans self.pdf
    * La représentation html est stockée sous forme d'une liste de
    * couples (numéro de page,contenu)
    """

    def __init__(self,filename,force=False):
        self.source_tex = filename
        self.editeur=False
        self.log=""
        self.latex_options = ['-interaction','nonstopmode']

        if not force:
            self.latex_options.append('-halt-on-error')

    def compiler(self):
        #On ne peut compiler sans fichier source
        if not self.source_tex:
            raise exception.CompilationError("Pas de fichier source à compiler")

        #1) Génère le pdf
        self.ret = utils.enter('tmp')
        self.tmpdir = os.path.abspath(os.curdir)

        print(os.path.abspath(self.source_tex))
        try:
            #print ('pdflatex',) + tuple(self.latex_options) + (self.source_tex,)
            #from time import sleep
            #sleep(60)
            x = subprocess.Popen(('pdflatex',) + tuple(self.latex_options) + (self.source_tex,),
                                 stderr=utils.dev_null(), stdout=subprocess.PIPE)
            retour = x.wait()
        except OSError:
            self.log = 'PdfLatex ne semble pas être installé'
            raise exceptions.CompilationError(self.log)
        else:
            tete_chemin = os.path.split(self.source_tex)[1]
            if retour:
                garde_fin = re.compile(r'\n(!.*)',re.DOTALL)
                self.log = open(tete_chemin.replace('.tex','.log')).read()
                print((self.log))
                self.log = garde_fin.search(self.log).group(1)
                raise exceptions.CompilationError
            else:
                curdir = os.path.abspath(os.curdir)
                self.pdf = '%s/%s' % (curdir, tete_chemin.replace('.tex','.pdf'))


        #2) Génère du code html

        self.html = utils.tex2inner(open(self.source_tex).read())
#        except OSError:
#            self.log = "Error invoking latex2html, maybe latex2html is not installed ?"
#        else:
        self.log = False

        return True


    def menage(self):
        # Should not be ran before the PDF gets copied
        #if os.path.abspath(os.curdir) == self.tmpdir:
        utils.leave(self.ret)
