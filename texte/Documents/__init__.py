#! /usr/bin/python
# -*- coding: utf-8 -*-


__all__ =['latex','exceptions']



def abstract(filename):
    from . import latex
    EXTENSIONS = (
                 (('tex','latex'),latex),
                 )

    for handler in EXTENSIONS:
        try:
            for ext in handler[0]:
                if filename.endswith(ext):
                    cur_handler = handler[1] 
                    break
        except TypeError:
            raise TypeError("Le format de %s n'est pas geré par LettraVox" % filename)
        return cur_handler.Abstraction(filename)
