import django.forms as forms
from django.db import models

from texte.models import *

class AjoutCommentaire(forms.ModelForm):
    auteur = forms.CharField(label="Nom", max_length=50)
    
    class Meta:
        model = Commentaire
        exclude = ('referent', 'pub_date', 'antispam')

class AjoutCommentaireCompact(AjoutCommentaire):
    texte = forms.CharField(label="Commentaire",
        widget=forms.Textarea(attrs={'cols':'10'}))
