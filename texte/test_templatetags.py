import os
from os.path import join
import shutil
import tempfile

from tagging.models import Tag

from .templatetags.lvtags import (
    samples,
    zoom_square,
)


def test_samples(db):
    sample_from_list = samples([1, 2, 3], 2)
    assert len(sample_from_list) == 2

    Tag.objects.create(name='foo')
    Tag.objects.create(name='bar')

    sample_from_qs = samples(Tag.objects.all(), 2)

    assert len(sample_from_qs) == 2


def test_zoom_square(settings):
    with tempfile.TemporaryDirectory() as tempdir:
        original_image_path = join(settings.MEDIA_ROOT, 'portraits/default.png')
        settings.MEDIA_ROOT = tempdir
        tmp_image_path = join(tempdir, 'category/test.png')
        tmp_image_url = join(settings.MEDIA_URL, 'category/test.png')
        os.mkdir(join(settings.MEDIA_ROOT, 'category/'))
        shutil.copy(original_image_path, tmp_image_path)
        miniature_url = zoom_square(tmp_image_url)

    # Barely checks if no crash
    assert miniature_url.endswith('.png')
