# -*- coding: utf-8 -*-


from django.core.management import call_command
from django.db import migrations, models


def load_initial_fixture(apps, schema_editor):
    call_command('loaddata', 'texte/fixtures/initial_data.json', **{'verbosity': 0})


class Migration(migrations.Migration):

    dependencies = [
        ('texte', '0003_auto_20170905_1605'),
    ]

    operations = [
        migrations.RunPython(load_initial_fixture)
    ]
