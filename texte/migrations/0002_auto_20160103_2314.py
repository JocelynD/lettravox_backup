# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('texte', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='htmltex',
            name='parent',
            field=models.OneToOneField(to='texte.Texte', on_delete=models.CASCADE),
        ),
    ]
