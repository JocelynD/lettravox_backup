# -*- coding: utf-8 -*-


from django.db import models, migrations
import datetime
from django.conf import settings
import tagging.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AuteurExterne',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nom', models.CharField(help_text=b"Nom ou pseudonyme de l'auteur", max_length=60, verbose_name=b'Nom')),
                ('url', models.URLField(help_text=b"Lien vers une page du site de l'auteur, la page  wikimedia commons ou autre...", blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CategorieReferenceExterne',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nom_court', models.CharField(help_text=b'ex: Achat', max_length=20)),
                ('nom_long', models.CharField(help_text=b'ex: Achetter ce texte sur papier', max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Commentaire',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('auteur', models.CharField(max_length=50)),
                ('email', models.EmailField(help_text=b'(optionnel, ne sera pas publi\xc3\xa9)', max_length=50, null=True, blank=True)),
                ('pub_date', models.DateTimeField(verbose_name=b'Date')),
                ('texte', models.TextField(max_length=500)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nom', models.CharField(max_length=40, verbose_name=b'Genre Litt\xc3\xa9raire')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='HtmlTex',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Illustration',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fichier', models.ImageField(upload_to=b'illustrations')),
                ('square_x_pos', models.IntegerField(default=-1)),
                ('square_y_pos', models.IntegerField(default=-1)),
                ('square_size', models.IntegerField(default=-1)),
                ('square', models.ImageField(upload_to=b'illustrations/zoomed', verbose_name=b'Aper\xc3\xa7u Carr\xc3\xa9', blank=True)),
                ('auteur', models.ForeignKey(blank=True, to='texte.AuteurExterne', help_text=b"Laisser vide si jamais vous \xc3\xaates l'auteur de l'illustration", null=True, on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Licence',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nom', models.CharField(max_length=120)),
                ('url', models.URLField()),
                ('image', models.ImageField(upload_to=b'licences')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Lien',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nom', models.CharField(max_length=200)),
                ('url', models.URLField()),
                ('description', models.TextField(max_length=500, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Modele',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nom', models.CharField(max_length=200)),
                ('fichier_source', models.FileField(upload_to=b'modele_src')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('no', models.SmallIntegerField()),
                ('page_suivante', models.BooleanField(default=False)),
                ('page_precedente', models.BooleanField(default=False)),
                ('texte', models.TextField()),
                ('html_repr', models.ForeignKey(to='texte.HtmlTex', on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProfilAuteur',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('photo', models.ImageField(default=b'portraits/default.png', upload_to=b'portraits', blank=True, help_text=b'Cette photo appara\xc3\xaetra sur le site \xc3\xa0 c\xc3\xb4t\xc3\xa9 de vos textes notamment', null=True)),
                ('anniv', models.DateField(null=True, verbose_name=b'Date de naissance', blank=True)),
                ('influences', models.CharField(max_length=100, null=True, blank=True)),
                ('localisation', models.CharField(max_length=50, null=True, blank=True)),
                ('hobies', models.CharField(max_length=140, null=True, verbose_name=b"Centres d'int\xc3\xa9r\xc3\xaat", blank=True)),
                ('aut_email', models.BooleanField(default=True, help_text=b'Si cette option est activ\xc3\xa9e, les visiteurs pourront vous envoyer des emails directement sur votre boite e-mail.', verbose_name=b'Adresse courriel publique')),
                ('aut_formulaire', models.BooleanField(default=True, help_text=b'Si cette option est activ\xc3\xa9e, les visiteurs pourront vous envoyer des mails \xc3\xa0 travers    un formulaire en ligne, ils ne verront pas votre v\xc3\xa9ritable adresse', verbose_name=b'Formulaire de contact')),
                ('site_perso', models.URLField(null=True, verbose_name=b'Site Perso', blank=True)),
                ('complement', models.TextField(help_text=b'Un petit texte de quelques lignes qui sera affich\xc3\xa9 \xc3\xa0 c\xc3\xb4t\xc3\xa9 de vos textes', max_length=800, null=True, verbose_name=b'Description', blank=True)),
                ('url_don', models.URLField(help_text=b'Permet de proposer au visiteur de vous faire un don (par exemple via paypal)', null=True, verbose_name=b'Url de don', blank=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ReferenceExterne',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.URLField()),
                ('classe', models.ForeignKey(to='texte.CategorieReferenceExterne', on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Section',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('page_debut', models.SmallIntegerField()),
                ('titre', models.CharField(max_length=200)),
                ('html_repr', models.ForeignKey(to='texte.HtmlTex', on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Texte',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titre', models.CharField(max_length=200)),
                ('slug', models.SlugField()),
                ('pub_date', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Date de publication')),
                ('fichier_source', models.FileField(help_text=b'Laisser vide pour \xc3\xa9diter un nouveau fichier en ligne', upload_to=b'text_src', blank=True)),
                ('log', models.TextField(verbose_name=b"Journal d'erreurs", max_length=4000, editable=False, blank=True)),
                ('est_compile', models.BooleanField(default=False, verbose_name=b'Texte compil\xc3\xa9')),
                ('est_publie', models.BooleanField(default=True, help_text=b"Tant que cette option n'est pas s\xc3\xa9lectionn\xc3\xa9e, le texte ne sera pas visible des visiteurs. Utile par exemple pour un brouillon", verbose_name=b'Texte Publi\xc3\xa9')),
                ('resume', models.TextField(max_length=1500, verbose_name=b'R\xc3\xa9sum\xc3\xa9', blank=True)),
                ('tags', tagging.fields.TagField(help_text=b'Tags s\xc3\xa9par\xc3\xa9s par des virgules', max_length=255, blank=True)),
                ('auteur', models.ForeignKey(to=settings.AUTH_USER_MODEL, blank=True, on_delete=models.CASCADE)),
                ('genre', models.ForeignKey(blank=True, to='texte.Genre', null=True, on_delete=models.SET_NULL)),
                ('illustration', models.ForeignKey(blank=True, to='texte.Illustration', null=True, on_delete=models.SET_NULL)),
                ('licence', models.ForeignKey(default=2, to='texte.Licence', on_delete=models.CASCADE)),
            ],
            options={
                'ordering': ['-id'],
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='referenceexterne',
            name='texte',
            field=models.ForeignKey(to='texte.Texte', on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='htmltex',
            name='parent',
            field=models.ForeignKey(to='texte.Texte', unique=True, on_delete=models.CASCADE),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='commentaire',
            name='referent',
            field=models.ForeignKey(to='texte.Texte', on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
