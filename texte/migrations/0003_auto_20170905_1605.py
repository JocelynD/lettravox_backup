# -*- coding: utf-8 -*-


from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('texte', '0002_auto_20160103_2314'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profilauteur',
            name='user',
            field=models.OneToOneField(related_name='profile', to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE),
        ),
    ]
