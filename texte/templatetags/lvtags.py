#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import random
from PIL import Image
import urllib.parse

from django.template import Library
from django.conf import settings
from django.apps import apps
from django.template import Node, TemplateSyntaxError
from tagging.models import Tag
from tagging.utils import calculate_cloud
from django.contrib.auth.models import User
from django.utils.safestring import mark_safe
from django.db import models
import html2text

import texte
import articles

register = Library()


@register.simple_tag
def random_banniere():
    return random.choice(settings.BANNERS)

@register.filter()
def thumbnail(src_file, size='48x48'):
    """Retourne l'url d'une image miniature. La miniature est créée si
    jamais elle n'existe pas. les miniatures se situent dans
    miniatures/image_HxL.jpeg , (H et L sont la hauteur et la largeur
    de la miniature) ce chemin étant relatif au chemin de l'image
    originale. Requiert la Python Imaging Library"""
    # Script inspiré de http://batiste.dosimple.ch/blog/2007-05-13-1/

#     print "ZOOM ",
#     print src_file,
#     print  "  is ",
#     print type(src_file)


    if not src_file:
        return

    if type(src_file) == models.fields.files.ImageFieldFile:
        original_image_path = src_file.path
    else:
        print((src_file.split(settings.MEDIA_URL)))
        original_image_path = src_file.split(settings.MEDIA_URL)[1]

    # defining the size
    x, y = [int(x) for x in size.split('x')]
    # Définition des diférents chemins...
    original_full_image_path = os.path.join(settings.MEDIA_ROOT,original_image_path)
    basename, format = original_image_path.rsplit('.', 1)
    name = basename.rsplit('/', 1)[1]
    basename = os.path.join(settings.MEDIA_ROOT,basename.rsplit('/', 1)[0])
    miniature = basename + '/miniatures/' + name + '_' + size + '.' +  format

    if not os.path.exists(basename + '/miniatures/'):
        os.mkdir(basename + '/miniatures/')

    miniature_filename = os.path.join(settings.MEDIA_ROOT, miniature)
    miniature_url = settings.MEDIA_URL + miniature.rsplit(settings.MEDIA_ROOT)[1]
    # if the image wasn't already resized, resize it
    if not os.path.exists(miniature_filename) \
        or os.path.getmtime(original_full_image_path) > os.path.getmtime(miniature_filename) :
        filename = os.path.join(settings.MEDIA_ROOT, original_image_path)
        image = Image.open(filename)
        image.thumbnail((x,y), Image.ANTIALIAS)
        image.save(miniature_filename, image.format)
    return miniature_url


@register.filter()
def zoom_square(src_file, param='0.4_0.5x0.5'):
    """
    le paramètre est de format "taille_position"
    - La taille est relative au côté le moins large (3 -> 0.3)
    - La position est celle du coin haut droit.
      ex :5*5 :
       - coin haut-droit à 0.5*largeur du bord gauche de l'image d'origine
       - coin haut-droit à 0.5*largeur du bord haut de l'image d'origine

    Retourne l'url d'une image zoomée et découpée au carré. La miniature est créée si
    jamais elle n'existe pas. les miniatures se situent dans
    miniatures/image_zsize_pos.jpeg ,
    de la miniature) ce chemin étant relatif au chemin de l'image
    originale. Requiert la Python Imaging Library"""
    # Script inspiré de http://batiste.dosimple.ch/blog/2007-05-13-1/

    print(("ZOOM {} is {}".format(src_file, type(src_file))))

    if not src_file:
        return

    if type(src_file) == models.fields.files.ImageFieldFile:
        original_image_path = src_file.path
    else:
        original_image_path = src_file.split(settings.MEDIA_URL)[1]


    # Définition des diférents chemins...
    original_full_image_path = os.path.join(settings.MEDIA_ROOT,original_image_path)
    basename, format = original_image_path.rsplit('.', 1)
    name = basename.rsplit('/', 1)[1]
    basename = os.path.join(settings.MEDIA_ROOT,basename.rsplit('/', 1)[0])

    miniature = basename + '/zoomed/' + name + '_z' + param + '.' +  format

    if not os.path.exists(basename + '/zoomed/'):
        os.mkdir(basename + '/zoomed/')

    miniature_filename = os.path.join(settings.MEDIA_ROOT, miniature)
    miniature_url = settings.MEDIA_URL + miniature.rsplit(settings.MEDIA_ROOT)[1]

    # if that zoomed version doesnt exists
    if not os.path.exists(miniature_filename) \
        or os.path.getmtime(original_full_image_path) > os.path.getmtime(miniature_filename) :
        filename = os.path.join(settings.MEDIA_ROOT, original_image_path)
        image = Image.open(filename)

        orig_w, orig_h = image.size

        # defining the size
        width, pos = param.split("_")
        print(width)
        print(pos)

        square_side = int(float(width)*min(orig_w, orig_h))
        pos_x, pos_y = [float(x) for x in pos.split('x')]
        print(pos_x)
        print(pos_y)

        d_left  = int(pos_x*orig_w)
        d_right = d_left + square_side
        d_top   = int(pos_y*orig_h) - square_side
        d_bottom= d_top + square_side

        image = image.crop((d_left, d_top, d_right, d_bottom))
        image.save(miniature_filename, image.format)
    return miniature_url


@register.filter()
def affiche_nom(auteur):
    """ Affiche Le nom d'un utilisateur de manière optimale : le nom
    et le prénom en majuscule si disponible. Sinon, seul le username
    sera affiché.
    """
    prenom = auteur.first_name
    nom = auteur.last_name

    if nom and prenom:
        return ("%s %s")%(prenom.capitalize(),nom.capitalize())
    elif prenom:
        return prenom.capitalize()
    else:
        return auteur.username.capitalize()


from datetime import date
@register.filter()
def age(date_naissance):
    """ A partir d'un objet datetime, calcule l'âge.
    """
    age = date.today() - date_naissance
    return "%i Ans"%int(age.days / 365.25)


@register.filter()
def samples(sequence, number):
    """ Renvoie un nombre elements d'éléments aléatoires d'une liste
    """
    if sequence:
        if isinstance(sequence, models.QuerySet):
            return sequence.order_by('?')[:int(number)]
        else:
            return random.sample(sequence, min(int(number), len(sequence)))
    else:
        return ()


@register.filter()
def antispam(email):
    """ Obscurcit un email afin de pouvoir l'afficher.
        * la classe f2 pour les caractères normaux
        * la classe f1 pour les caractères à ne pas afficher.

        Dans le code css:
         .fool {display:none;}
    """
    c = 0
    mail_len =  len(email)
    out = ''

    while c < mail_len:
        if random.choice((True, False)):
            out += '<span class="f2">%c</span>' % email[c]
            c += 1
        else:
            # Permet d'avoir un caractère aléatoire
            out += '<span class="f1">%c</span>' % chr(random.randrange(122))

    return mark_safe(out)
antispam.is_safe = True


@register.inclusion_tag('texte/tags/liste_auteurs.html')
def liste_auteurs():
    return {'auteurs': User.Auteurs.all()}


@register.inclusion_tag('texte/tags/derniers_textes.html')
def derniers_textes(n_textes=5):
    tous_textes = texte.models.Texte.objects.published().order_by('-pub_date')
    try:
        derniers_textes = tous_textes[:n_textes-1]
    except KeyError:
        derniers_textes = tous_textes
    return {'liste_textes': derniers_textes}

@register.inclusion_tag('texte/tags/blocs_derniers_textes.html')
def blocs_derniers_textes(n_textes=3):
    tous_textes = texte.models.Texte.objects.published().order_by('-pub_date')
    try:
        derniers_textes = tous_textes[:n_textes]
    except KeyError:
        derniers_textes = tous_textes
    return {'liste_textes': derniers_textes}


@register.inclusion_tag('texte/tags/liste_liens.html')
def liste_site_amis():
    return {'site_amis': texte.models.Lien.objects.all()}

@register.inclusion_tag('admin/texte/tags/liste_modeles.html')
def liste_modeles():
    return {'modeles': texte.models.Modele.objects.all()}

@register.inclusion_tag('tagging/tags/nuage_tags.html')
def nuage_tags_texte(args=0.4):
    """
    Takes either:
     * arg=<font-size>
     * arg=<font-size>,<limit_to> where limit_to is the number of
       displayed tags, randomly selected.
    """
    try:
        taille, limit_to = args.split(',')
        #print limit_to
        return {'taille': taille, 'limit_to': limit_to}
    except AttributeError:
        return {'taille':args}




@register.inclusion_tag('disp_chaine.html')
def nom_site():
    return {'chaine':settings.SITE_NAME}

@register.inclusion_tag('disp_chaine.html')
def url_site():
    return {'chaine':settings.SITE_URL}

class TagsCloudForQuerySetNode(Node):
    def __init__(self, dotted_path: str, varname: str, step):
        """
        Builds a tag cloud for a QuerySet

        :param dotted_path: path to a dotted model form (eg: ``app.ModelName``)
          followed by method/attr path leading to a queryset.
          ``app.ModelName.objects.published`` for eg.
        :param step: how many different tag size do we want
        """
        self.varname, self.step = varname, step
        self.dotted_path = dotted_path

    @staticmethod
    def _get_qs(dotted_path: str) -> models.QuerySet:

        bits = dotted_path.split('.')

        model_path = bits[:2]
        queryset_path = bits[2:]
        if len(model_path) < 2:
            raise ValueError(f'{bits} is not a valid model dotted path')

        model = apps.get_model(*model_path)

        if not queryset_path:
            raise ValueError(
                'No path to queryset, you might want to use TagsCloudForModel')

        # can crawl several levels like "Foo.objects.published"
        obj = model
        for attrname in queryset_path:
            if hasattr(obj, attrname):
                attr = getattr(obj, attrname)
                if callable(attr):
                    obj = attr()
                else:
                    obj = attr
        return obj

    def render(self, context: dict):
        qs = self._get_qs(self.dotted_path)
        tags = Tag.objects.usage_for_queryset(qs, self.step)
        context[self.varname] = calculate_cloud(tags, self.step)
        return ''

@register.tag('tags_cloud_for_queryset')
def do_tags_cloud_for_queryset(parser, token):
    """
    Retrieves a tag cloud for a given queryset

    Example usage::

        {% tags_cloud_for_queryset app.Model.path.objects.published as object_list %}
    """
    step = 4
    bits = token.contents.split()
    if len(bits) < 4:
        raise TemplateSyntaxError('%s tag requires three arguments' % bits[0])
    if bits[2] != 'as':
        raise TemplateSyntaxError("second argument to %s tag must be 'as'" % bits[0])
    if len(bits) > 4:
        if bits[4] != 'step':
            raise TemplateSyntaxError("optional fourth argument to %s tag must\
                                       be 'step'" % bits[0])
        try:
            step = int(bits[5])
        except IndexError:
            raise TemplateSyntaxError("optional fifth argument after step is \
                                       required")
        except ValueError:
            raise TemplateSyntaxError("optional fifth argument after step is \
                                       not an integer")
    return TagsCloudForQuerySetNode(bits[1], bits[3], step)



@register.filter()
def fois(n1,n2):
    return str(float(n1) * float(n2))


@register.inclusion_tag('articles/tags/vitrine.html')
def derniers_articles(n_articles=10):
    # FIXME: sqlite ne semble pas vouloir faire de order_by datetime ??
    tous_articles = articles.models.Article.objects.published().order_by('-pub_date')
    try:
        derniers_articles = tous_articles[:n_articles-1]
    except KeyError:
        derniers_articles = tous_articles
    return {'derniers_articles': derniers_articles}


@register.inclusion_tag('articles/tags/ruban.html')
def ruban_articles():
    """Pioche au hasard parmis les articles mis en valeur
    """
    all_hl = articles.models.Article.objects.published().filter(highlight=True)
    if len(all_hl) > 0:
        return {'highlighted': all_hl.order_by('?')[0]}
    else:
        return {'highlighted': None}

@register.filter()
def plaintext(html):
    """ Converts HTML content to plaintext, suitable for email
    """
    h = html2text.HTML2Text()
    h.ignore_images = True
    h.inline_links = False
    return h.handle(html).strip()


@register.filter
def absolutize(path):
    """ Adds domain and protocol to url
    """
    return urllib.parse.urljoin(settings.SITE_URL, path)


@register.simple_tag
def all_newsletters():
    profiles_qs = texte.models.ProfilAuteur.objects.exclude(newsletter_url='')
    return profiles_qs.values('newsletter_url', 'newsletter_text')
