from django.http import HttpRequest


def global_authors_info(request: HttpRequest):
    from django.contrib.auth.models import User

    authors = User.Auteurs.all()
    return dict(
        authors=authors,
        has_multiple_authors=(authors.count() > 1),
    )
