# -*- coding: utf-8 -*-
import os
import shutil
from datetime import datetime
from PIL import Image

from django.db import models
from django.conf import settings
from django.contrib import auth

# Django 1.1 and 1.4 compat
try:
    from threading import local
except ImportError:
    from django.utils._threading_local import local

from django.utils.safestring import mark_safe
from django.template import Template, Context

from tagging.fields import TagField
from slughifi import slugify

from . import Documents
from texte.templatetags import lvtags


class Auteurs(models.Manager):
    """Ne renvoie que les utilisateurs ayant publié au moins un texte
    """
    def __init__(self):
        super(Auteurs, self).__init__()
        self.model = auth.models.User

    def get_queryset(self):
        return super(Auteurs, self).get_queryset().filter(profile__is_author=True)

auth.models.User.Auteurs = Auteurs() # FIXME: ugly


def default_photo():
    return settings.DEFAULT_PHOTO


class ProfilAuteur(models.Model):
    """Utilise le mécanisme de profil associé aux utilisateurs
    Django pour gérer des infos supplémentaires sur les utilisateurs
    """
    is_author = models.BooleanField(
        'est un·e auteur·ice',
        help_text='Lister cet utilisateur comme un·e auteur·ice dans le site public',
        null=False,
        default=True)
    user = models.OneToOneField(
        auth.models.User, related_name='profile',
        on_delete=models.CASCADE)
    photo = models.ImageField(
        upload_to="portraits",
        blank=True,
        null=True,
        default=default_photo,
        help_text="Cette photo apparaîtra sur le site à côté de vos textes notamment")
    anniv = models.DateField('Date de naissance',blank=True,null=True)
    influences = models.CharField(max_length=100,blank=True,null=True)
    localisation = models.CharField(max_length=50,blank=True,null=True)
    hobies = models.CharField("Centres d'intérêt",max_length=140,blank=True,null=True)
    aut_email = models.BooleanField('Adresse courriel publique',
        default=True,
        help_text="Si cette option est activée, les visiteurs pourront vous envoyer des emails directement sur votre boite e-mail.")
    aut_formulaire = models.BooleanField('Formulaire de contact',
        default=True,
        help_text="Si cette option est activée, les visiteurs pourront vous envoyer des mails à travers    un formulaire en ligne, ils ne verront pas votre véritable adresse")
    website_url = models.URLField(
        "URL du Site Perso ",
        help_text="…ou réseau social. Laisser vide si pas de site perso",
        blank=True, null=True, default='',
    )
    website_text = models.CharField(
        "Texte du lien vers le site web",
        help_text="Si vide, l'URL sera utilisé en tant que texte",
        blank=True, null=False, default='', max_length=100,
    )
    newsletter_url = models.URLField(
        "URL d'abonnement à la lettre d'infos de l'auteure",
        help_text="Laisser vide si pas de lettre d'info",
        blank=True, null=False, default='',
    )
    newsletter_text = models.CharField(
        "Texte du lien d'abonnement à la lettre d'info de l'auteure",
        help_text="Adresse externe (ex: mailchimp). Laisser vide si pas de lettre d'info",
        blank=True, null=False, default='', max_length=100)

    complement = models.TextField("Description",
        max_length=800,
        blank=True,
        null=True,
        help_text="Un petit texte de quelques lignes qui sera affiché à côté de vos textes")
    url_don = models.URLField('URL de don',
        blank=True,null=True,
        help_text="Permet de proposer au visiteur de vous faire un don (par exemple via paypal)")

    @classmethod
    def get_default(cls):
        """Renvoie l'objet User choisi dans la configuration
        ou bien None si il n'existe pas ou n'est pas défini
        """
        return auth.models.User.objects.get(username=settings.DEFAULT_USERNAME)
        try:
            return auth.models.User.objects.get(username=settings.DEFAULT_USERNAME)
        except auth.models.User.DoesNotExist:
            return None
        except NameError:
            return None


    def get_photo_abspath(self):
        if self.photo:
            return self.photo.path
        else:
            return ''

    def get_email(self):
        #FIXME: certainement useless, travail du template.
        if self.aut_email:
            return lvtags.antispam(self.user.email)
        else:
            return ''

    def __str__(self):
        if self.user.first_name:
            return "%s %s" % (self.user.first_name, self.user.last_name)
        else:
            return self.user.username

    def ses_textes(self):
        return self.user.texte_set.published()


class Genre(models.Model):
    """ Définit un genre littéraire """
    nom = models.CharField('Genre Littéraire',max_length=40)

    def __str__(self):
        return self.nom

class AuteurExterne(models.Model):
    nom = models.CharField('Nom', max_length=60,
                           help_text='Nom ou pseudonyme de l\'auteur')
    url = models.URLField(blank=True,
                          help_text="Lien vers une page du site de l'auteur, la page  wikimedia commons ou autre...")

    def __str__(self):
        return self.nom

class Illustration(models.Model):
    #url = models.URLField(help_text="Lien vers\
    #une page du site de l'auteur, la page  wikimedia commons\
    #ou autre...")
    auteur = models.ForeignKey(
        AuteurExterne, blank=True, null=True,
        on_delete=models.CASCADE,
        help_text="Laisser vide si jamais vous êtes l'auteur·e de l'illustration")
    fichier = models.ImageField(upload_to="illustrations")
    square_x_pos = models.IntegerField(default = -1)
    square_y_pos = models.IntegerField(default = -1)
    square_size = models.IntegerField(default = -1)
    square = models.ImageField("Aperçu Carré",upload_to="illustrations/zoomed", editable=True, blank=True,
                               help_text="cliquer sur l'illustration pour modifier la zone de l'aperçu carré")

    def legende(self):
        ret = "Illustration : "
        if self.auteur:
            ret += str(self.auteur)
        else:
            ret += str(self.texte_set.all()[0].auteur)

        return ret

    def set_default_square(self):
        min_side = self.fichier.width
        if self.fichier.height < min_side:
            min_side = self.fichier.height

        self.square_size = int(min_side*0.4)
        self.square_x_pos = int(0.5*self.fichier.width - self.square_size)
        self.square_y_pos = int(0.5*self.fichier.height - self.square_size)

    def save(self, *args, **kwargs):
        if  self.square_size == None or self.square_size == -1:
            self.set_default_square()
        if not self.id:
            super(Illustration, self).save(*args, **kwargs)


        # workaround,
        # When saving,
        try:
            # first time, path is illustration/sth.jpg
            base, fichier = self.fichier.name.rsplit('/',1)
        except ValueError:
            # changing: sth.jpg
            fichier = self.fichier.name
            base = 'illustrations'

        self.square = os.path.join(base, 'zoomed', fichier)
        super(Illustration, self).save()
        image = Image.open(self.fichier.path)
        d_left = self.square_x_pos
        d_right = self.square_x_pos + self.square_size
        d_top = self.square_y_pos
        d_bottom = self.square_y_pos + self.square_size
        image = image.crop((d_left, d_top, d_right, d_bottom))
        image.save(self.square.path, image.format)

    def __str__(self):
        return os.path.split(str(self.fichier))[-1]


class TextesQuerySet(models.QuerySet):
    def published(self):
        return self.filter(est_compile=True, est_publie=True, pub_date__lte=datetime.now())


class Texte(models.Model):
    """ Contient un texte publié, sa description, sa source Tex, sa
    représentation html & pdf, ses métadonnées et ses commentaires
    """
    auteur = models.ForeignKey(
        auth.models.User,
        blank=True, on_delete=models.CASCADE)
    titre = models.CharField(max_length=200)
    slug = models.SlugField(unique=True)
    pub_date = models.DateTimeField('Date de publication',default=datetime.now)
    fichier_source = models.FileField(
        help_text="Laisser vide pour éditer un nouveau fichier en ligne",
        upload_to="text_src",blank=True)
    log = models.TextField("Journal d'erreurs", max_length=4000,
                           blank=True, editable=False)
    est_compile = models.BooleanField('Texte compilé',default=False)
    est_publie = models.BooleanField('Texte Publié',default=True,
                                     help_text="Tant que cette option n'est pas sélectionnée, le texte ne sera pas visible des visiteurs. Utile par exemple pour un brouillon")
    resume = models.TextField("Résumé", max_length=1500, blank=True)
    licence = models.ForeignKey(
        'Licence', default=settings.DEFAULT_LICENCE,
        on_delete=models.CASCADE)
    illustration = models.ForeignKey(
        Illustration,
        blank=True, null=True, on_delete=models.SET_NULL)
    genre = models.ForeignKey(Genre, blank=True, null=True, on_delete=models.SET_NULL)
    tags = TagField(help_text="Tags séparés par des virgules")

    class Meta:
        ordering = ['-id']

    objects = TextesQuerySet.as_manager()

    def __str__(self):
        return self.titre

    def is_just_published(self):
        """ Will that model gets published state on next save() ?
        """
        new_publication_status = self.est_publie and self.est_compile

        if self.pk: # is it a pre-existing instance in db ?
            old_self = self.__class__.objects.get(pk=self.pk)
            old_publication_status = old_self.est_publie and old_self.est_compile


            if new_publication_status and not old_publication_status:
                just_published = True
            else:
                just_published = False
        else:
            just_published = new_publication_status
        return just_published

    def save(self, *args, **kwargs):
        self.slug = slugify(self.titre)
        self.just_published = self.is_just_published()
        super(Texte, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return "/textes/%s/"%self.slug

    def get_absolute_pdf_path(self):
        path = self._build_pdf_path()

        if os.access(path, os.R_OK):
            return path
        else:
            raise IOError

    def is_unipage(self):
        return self.htmltex.page_set.count() == 1

    def _build_pdf_path(self):
        if self.fichier_source:
            base = os.path.splitext(os.path.basename(self.fichier_source.path))[0]
            return "%s/%s.pdf" % (settings.PDF_ROOT, base)
        else:
            return False

    def _source_path(self):
        """Retourne le chemin complet du fichier source sur le système
        de fichiers.
        """
        if self.fichier_source:
            return self.fichier_source.path
        else:
            return False

    def source_base(self):
        """ Retourne le nom du fichier, sans le chemin complet.
        """
        if self.fichier_source:
            return os.path.split(self.fichier_source.path)[1]
        else:
            return 'Nouveau texte'

    def get_source(self):
        if self.fichier_source:
            return open(self._source_path()).read()
        else:
            return ''

    def save_source(self,content):
        """
        Enregistre le contenu de content dans le fichier attaché au
        Texte. Si aucun fichier n'existe, un fichier sera créé. Son
        nom sera du type texte_src/<slug>.tex
        Où <slug> est le champ slug de l'instance concernée de l'objet
        Texte.
        """
        if self.fichier_source:
            fpath = self._source_path()
            try:
                os.rename(fpath,'%s~'%fpath)
            except:
                print(("echec de sauvegarde de %s "%fpath))
        else:
            self.fichier_source = 'text_src/%s.tex'%self.slug
            self.save()
            fpath = self._source_path()
        fichier_source = open(fpath,'w')
        fichier_source.write(content.strip())
        fichier_source.close()
        self.est_compile = False
        self.save()

    def nombre_com(self):
        return self.commentaire_set.count()
        nombre_com.short_description = 'Nombre de commentaires'

    def nombre_pages(self):
        return self.htmltex.page_set.count()
        nombre_pages.short_description = 'Nombre de pages'

    def compiler(self):
        """ Génère le pdf, les éventuelles erreurs sont indiquées dans
        le champ log de l'instance de Texte concernée.
        """
        chemin_source = os.path.join(settings.MEDIA_ROOT,str(self.fichier_source))
        objet_source = Documents.abstract(chemin_source)
        self.log = ""
        try:
            objet_source.compiler()

        except Documents.exceptions.CompilationError as e:
            try:
                self.log = objet_source.log
            except AttributeError:
                self.log = e
            raise e
        except Exception as e:
            # Si l'on a pas de log de sortie, on se rabat sur le message d'erreur'
            self.log = e
            raise  Documents.exceptions.CompilationError(e)
        shutil.move(objet_source.pdf, self._build_pdf_path())

        # Supprime les fichiers temporaires
        objet_source.menage()


        """ Génère le html dans la base de données
        """

        texhash = objet_source.html

        try:
            representation = HtmlTex.objects.get(parent=self)
        except HtmlTex.DoesNotExist:
            representation = HtmlTex(parent=self)
        representation.save()
        representation.init(texhash)
        return True

    def bouton_compiler(self):
        """Permet d'afficher dans le détail de la page d'admin un lien
        pour compiler le pdf et le html d'un clic
        """
        if not self.fichier_source:
            return "pas de source !"
        warning = ""
        if not self.est_compile:
            warning = "<img alt='Attention !' src='/static/img/icon_alert.gif' />"
        return mark_safe('%s<a href="%s/compiler/">Compiler !</a>'%(warning,self.id))
    bouton_compiler.short_description='Compiler le texte'


class Commentaire(models.Model):
    """Gère les comentaires sur les textes
    """
    referent = models.ForeignKey(Texte, on_delete=models.CASCADE)
#    titre = models.CharField(max_length=1000)
    auteur = models.CharField(max_length=50)
    email = models.EmailField(max_length=50, blank=True, null=True, help_text=\
        "(optionnel, ne sera pas publié)")
    pub_date = models.DateTimeField('Date')
    texte = models.TextField(max_length=500)

    def __str__(self):
        return "%s par %s"%(self.auteur, self.pub_date)


class Section(models.Model):
    """
    Une section du texte par classe section, chaque Section doit être
    rattachée à une représentation HtmlTex : self.html_repr.
    """
    html_repr = models.ForeignKey('HtmlTex', on_delete=models.CASCADE)
    page_debut = models.SmallIntegerField()
    titre = models.CharField(max_length=200)

    def __str__(self):
        return self.titre + ' ' + self.html_repr.parent.titre


class HtmlTex(models.Model):
    """ Gère le fichier Tex du texte publié.
    """
    parent = models.OneToOneField(Texte, on_delete=models.CASCADE)

    def __str__(self):
        return self.parent.titre

    def get_absolute_url(self):
        return "%slire/" % self.parent.get_absolute_url()

    def init(self, data):
        self.section_set.all().delete()
        if not data['single']:
            for i in data['index']:
                Section.objects.create(
                    html_repr=self, titre=i[1], page_debut=i[0])

        # Retranche un si pas en mode single
        nb_pages = len(data['pages']) - 1
        i = 1

        while i <= nb_pages:
            if i > 1:
                precedent = True
            else:
                precedent = False

            if i < nb_pages:
                suivant = True
            else:
                suivant = False

            # Try to keep old pages, avoid deleting bug reports linked to them
            try:
                p = Page.objects.get(html_repr=self, no=i)
            except Page.DoesNotExist:
                p = Page(html_repr=self, no=i)

            p.texte = data['pages'][i]
            p.page_suivante = suivant
            p.page_precedente = precedent
            p.save()

            i += 1

        # Remove remaining pages from older compilations with more pages
        self.page_set.filter(no__gt=nb_pages).delete()

    def multipart(self):
        return len(self.page_set.all()) > 1




class Page(models.Model):
    """Une Page du texte par classe section, chaque Page doit être
    rattachée à une représentation HtmlTex : self.html_repr. la page
    est appellée par son n° : self.no
    """
    html_repr = models.ForeignKey(
        HtmlTex,
        blank=False, on_delete=models.CASCADE)
    no = models.SmallIntegerField()
    page_suivante = models.BooleanField(default=False)
    page_precedente = models.BooleanField(default=False)
    texte = models.TextField()

    def get_absolute_url(self):
        return "%s%i/" % (self.html_repr.get_absolute_url(), self.no)

    def __str__(self):
        return '%i de %s'%(self.no,self.html_repr.parent.titre)

    def get_texte_parent(self):
        return self.html_repr.parent



class Licence(models.Model):
    nom = models.CharField(max_length=120)
    url =  models.URLField()
    image = models.ImageField(upload_to="licences")

    def __str__(self):
        return self.nom


class Lien(models.Model):
    nom = models.CharField(max_length=200)
    url =  models.URLField()
    description = models.TextField(max_length=500,blank=True)

    def __str__(self):
        return self.nom


class Modele(models.Model):
    nom = models.CharField(max_length=200)
    fichier_source = models.FileField(upload_to="modele_src")

    def __str__(self):
        return self.nom

    def _source_path(self):
        return os.path.join(settings.MEDIA_ROOT,str(self.fichier_source))

    def get_source(self,texte):
        """Retourne le code du modele pre-rempli avec certains
        champs. les motifs à substituer dans le modèle apparaissent
        sous la form #MOTIF#
        """
        modele_code = open(self._source_path()).read()
        contexte = Context({'auteur':texte.auteur, 'request': None})
        nom_auteur = Template("{%load lvtags%}{{auteur|affiche_nom}}")

        #syntaxe : ("MOTIF",chaine_a_inclure)
        replace_coresp = (
            ('AUTEUR',nom_auteur.render(contexte)),
            ('TITRE',texte.titre),
            ('LICENCE',texte.licence.nom),
            ('WWW_LICENCE', texte.licence.url)
            )
        for i in replace_coresp:
            modele_code = modele_code.replace('#%s#'%i[0],i[1])
        return str(modele_code)


class CategorieReferenceExterne(models.Model):
    nom_court=models.CharField(help_text="ex: Achat",max_length=20)
    nom_long=models.CharField(help_text="ex: Achetter ce texte sur papier",max_length=100)

    def __str__(self):
        return self.nom_court


class ReferenceExterne(models.Model):
    """ Représente une référence à un texte donné sur un site externe.
    """
    #libelle = models.CharField('Libellé',max_length="150")
    url =  models.URLField()
    texte = models.ForeignKey(Texte, on_delete=models.CASCADE)
    classe = models.ForeignKey(
        CategorieReferenceExterne,
        on_delete=models.CASCADE)

    def __str__(self):
        return "%s : %s" % (self.texte, self.classe.nom_court)

#publie = TexteManager()
