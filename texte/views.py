#! /usr/bin/env python
# -*- coding: utf-8 -*-
import os
from datetime import datetime

from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.contrib.auth.decorators import permission_required
from django.conf import settings
from django.core.mail import send_mail
from django.views.generic import DetailView, ListView
from captchaproxy.decorators import captcha_protected
from tagging.models import Tag


import texte.Documents.exceptions
from texte import Documents
from texte.models import *
import texte.forms

class TextListView(ListView):
    queryset = Texte.objects.published().order_by('-pub_date')


class AuthorListView(ListView):
    queryset = Auteurs().all()


class AuthorDetailView(DetailView):
    queryset = Auteurs().all()
    slug_field = 'username'


class LinkListView(ListView):
    queryset = Lien.objects.all()


class TagDetailView(DetailView):
    queryset = Tag.objects.all()
    slug_field = 'name'


def telecharger(requete,slug):
    t = get_object_or_404(Texte, slug=slug)
    file_to_dl = t.get_absolute_pdf_path()
    try:
        response = HttpResponse(open(file_to_dl, 'rb'), "application/pdf")
    except IOError:
        raise Http404
    response['Content-Disposition'] = 'attachment; filename=%s.pdf' % t.slug
    return response

def page_en_ligne(requete,slug,no_page):
    if not no_page:
        no_page = 1
    t = get_object_or_404(Texte,slug=slug)
    r = get_object_or_404(HtmlTex,parent=t)
    p = get_object_or_404(Page, html_repr=r,no=int(no_page))

    if requete.POST:
        f = requete.POST['form']
    else:
        f = texte.forms.AjoutCommentaire()

    fw_valeurs = {'page':p,'object':t, 'form':f, 'user': requete.user}
    return render(requete, 'texte/page_detail.html',fw_valeurs)

@permission_required('texte.change_texte',login_url="/admin/")
def compiler(requete,id,**args):
    t = get_object_or_404(Texte,pk=id)

    if not t.fichier_source:
        log = 'Pas de fichier source'
        fw_valeurs = {'object':t,'log':log}

        return render(requete, 'admin/texte/voir_log.html',fw_valeurs)

    try:
        t.est_compile = t.compiler()
        if t.est_compile:
            t.save()
        else:
            raise Documents.exceptions.CompilationError

    # FIXME: manage to raise CompilationError
    except Documents.exceptions.CompilationError as e:
        fw_valeurs = {'object':t, 'referer': requete.META.get('HTTP_REFERER')}
        raise
        return render(requete, 'admin/texte/voir_log.html',fw_valeurs)

    else:
        return HttpResponseRedirect("/admin/texte/texte/%s/"%id)


@permission_required('texte.change_texte',login_url="/admin/")
def source_edit(requete, id, **args):
    t = get_object_or_404(Texte,pk=id)
    have_to_compile, fw_valeurs = generic_edit(requete, t)

    if not have_to_compile:
        return render(requete, 'admin/texte/editer_source.html', fw_valeurs)
    else:
        # fw_valeurs contient une requête.
        return fw_valeurs


def generic_edit(requete, text):
    """
    Renvoie deux types différents d'arguments.
    Soit il n'y a pas de compilation et generic_edit renvoie:
     False, <dictionnaire de template>
    Soit il y a une compilation et generic_edit renvoie:
     True, requête.
    """
    t = text
    havetosave, saved = False, False
    POST = {}
    for key in requete.POST.keys():
        POST.update({key:requete.POST[key]})

    if POST.__contains__('modele') and POST['modele']:
        id_modele = POST["modele"]
        try:
            code = get_object_or_404(Modele,pk=id_modele).get_source(t)
        except Http404:# Si le modèle n'existe pas
            code=''
        POST['code'] = code

    if POST.__contains__('code'):
        havetosave = True
        txt = requete.POST['code']
        try:
            t.save_source(POST['code'])
        except:
            saved = False
            raise
        else:
            saved = True
    fw_valeurs = {'object':t,'saved':saved,'save_attempt':havetosave, 'referer': requete.META.get('HTTP_REFERER')}

    if POST.__contains__('and_compile') and POST['and_compile'] == 'true':
        return True, texte.views.compiler(requete,t.id)

    else:
        return False, fw_valeurs


## Requêtes AJAX

def generic_commenter(requete, slug, commentaireForm):
    t = get_object_or_404(texte.models.Texte, slug=slug)

    if not requete.POST:
        f = commentaireForm()

    else:
        f = commentaireForm(requete.POST)

        if f.is_valid():
            commentaire = f.save(commit=False)
            commentaire.referent = t
            commentaire.pub_date = datetime.now()
            commentaire.save()

            delete_url = "%s/admin/texte/commentaire/%i/delete/" % (settings.SITE_URL, commentaire.id)

            corps = "%s a déposé un commentaire sur le texte %s :\n\n%s\n\n" % (commentaire.auteur, t, commentaire.texte) +\
                "Visualiser le commentaire : %s%s\n" % (settings.SITE_URL, t.get_absolute_url()) +\
                "Commentaire indésirable ? le supprimer : %s\n" % delete_url

            send_mail('[%s] Nouveau commentaire sur %s' % (settings.SITE_NAME, t),
                      corps, settings.DEFAULT_FROM_EMAIL,
                      [t.auteur.email], fail_silently=False)
            return True, {'form': f, 'texte': t, 'object' : t}

    # False correspond ou bien à des erreurs de remplissage ou bien au
    # premier affichage du formulaire.

    return False, {'form': f, 'texte': t, 'object' : t}

def ajax_commenter(requete, slug):
    success, vals =  generic_commenter(requete, slug, texte.forms.AjoutCommentaireCompact)
    if success:
        return render(requete, 'texte/ajax/commentaire_merci.html')
    else:
        return render(requete,'texte/ajax/commentaire_add.html', vals)

@captcha_protected
def commenter(requete, slug, no_page):
    success, vals =  generic_commenter(requete, slug, texte.forms.AjoutCommentaire)

    if success:
        return HttpResponseRedirect('/textes/%s/lire/%i/' % (vals['texte'].slug, int(no_page)))
    else:
        t = get_object_or_404(Texte,slug=slug)
        r = get_object_or_404(HtmlTex,parent=t)
        p = get_object_or_404(Page, html_repr=r,no=int(no_page))
        vals.update({'page': p})
        return render(requete, 'texte/page_detail.html', vals)
