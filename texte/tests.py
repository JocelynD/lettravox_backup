# -*- coding: utf-8 -*-


from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from tagging.models import Tag

from .models import Texte, Page, ProfilAuteur, HtmlTex
from .templatetags import lvtags


SIMPLE_TEX = r"""
\documentclass[a4paper,12pt]{article}
\usepackage[utf8x]{inputenc}
\usepackage[francais]{babel}
\begin{document}
\section*{Titre de la première partie}
    Texte blah blah
\end{document}
"""


class TexteTests(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='foo')

    def test_just_published(self):
        txt = Texte.objects.create(
            titre='foo',
            auteur=self.user
        )
        self.assertEqual(txt.just_published, False)
        txt.est_publie = True
        txt.est_compile = True
        txt.save()
        self.assertEqual(txt.just_published, True)

    def test_not_published(self):
        txt = Texte.objects.create(
            titre='foo',
            auteur=self.user
        )
        txt.titre = 'ahaha'
        txt.save()
        self.assertEqual(txt.just_published, False)


class AdminTests(TestCase):
    fixtures = ['initial_data']

    def setUp(self):
        super(TestCase, self).setUp()
        self.user = User.objects.create_superuser(
            username='admin',
            email='admin@example.com',
            password='password')
        self.client.login(username='admin', password='password')

    def test_publish_text(self):
        response = self.client.post('/admin/texte/texte/add/', {
            'titre': 'testé approuvé…',
            'pub_date_0': '2015-11-01',
            'pub_date_1': '23:12',
            'licence': 1,
            'referenceexterne_set-TOTAL_FORMS': 0,
            'referenceexterne_set-INITIAL_FORMS': 0,
        })
        assert response.status_code == 302
        assert Texte.objects.count() == 1

        editor_response = self.client.get('/admin/texte/texte/1/editeur/')
        assert editor_response.status_code == 200

        compile_response = self.client.post('/admin/texte/texte/1/editeur/', {
            'and_compile': 'true',
            'code': SIMPLE_TEX,
        })
        assert compile_response.status_code == 302
        assert Texte.objects.all()[0].est_compile is True

    def test_load_model(self):
        txt = Texte.objects.create(
            titre='foo',
            auteur=self.user
        )
        assert Texte.objects.count() == 1

        response = self.client.post('/admin/texte/texte/1/editeur/', {
            'and_compile': '',
            'code': '',
            'modele': 1,
        })
        assert response.status_code == 200
        # Template content is loaded
        assert "\\title{foo}" in response.content.decode()



class WebRenderingTests(TestCase):
    def setUp(self):
        super(TestCase, self).setUp()
        self.user = User.objects.create_superuser(
            username='admin',
            email='admin@example.com',
            password='password')
        self.client.login(username='admin', password='password')

    def _render(self, tex_content):
        text = Texte.objects.create(
            titre='coucou',
            fichier_source=SimpleUploadedFile('test_src.tex', tex_content),
            auteur=self.user,
        )
        text.compiler()
        return text

    def test_web_no_sub_toc(self):
        src = rb"""
\documentclass[a4paper,12pt]{article}
\usepackage[francais]{babel}
\usepackage[utf8x]{inputenc}
\begin{document}
\section*{}

\subsubsection*{foo}
    Texte blah blah

\subsubsection*{bar}

Foo foo bar
\end{document}"""

        text = self._render(src)
        page = text.htmltex.page_set.all()[0]
        self.assertNotIn('ChildLinks', page.texte)

    def test_compile_twice(self):
        text = Texte.objects.create(
            titre='coucou',
            fichier_source=SimpleUploadedFile(
                'test_src.tex', SIMPLE_TEX.encode('utf-8')),
            auteur=self.user,
        )
        # Barely checks that everything is ok while compiling twice
        text.compiler()
        text.compiler()
        # self.assertEqual(Texte.objects.count(), 1)
        # self.assertEqual(HtmlTex.objects.count(), 1)
        # self.assertEqual(Page.objects.count(), 1)


class PublicWebsiteTests(TestCase):
    def setUp(self):
        self.user = User.objects.create_superuser(
            username='admin1234',
            email='admin@example.com',
            password='password')

    def test_read_redirect_to_page(self):
        response = self.client.get('/textes/foo/lire/')
        self.assertEqual(response.status_code, 301)
        self.assertEqual(
            response.get('Location', None), '1/')

    def test_text_list(self):
        """ Just check no crash """
        response = self.client.get('/textes/')
        self.assertEqual(response.status_code, 200)

    def test_texte_download(self):
        """ Just check no crash """
        txt = Texte.objects.create(
            titre='foo',
            auteur=self.user,
            est_publie=True,
            est_compile=True,
            fichier_source=SimpleUploadedFile(
                'test_src.tex', SIMPLE_TEX.encode('utf-8')),
        )
        txt.compiler()

        response = self.client.get('/textes/foo/telecharger/')
        self.assertEqual(response.status_code, 200)

    def test_author_list(self):
        """ Just check no crash """
        response = self.client.get('/auteurs/')
        self.assertEqual(response.status_code, 200)

    def test_author_detail(self):
        response = self.client.get('/auteur/admin1234/')
        self.assertEqual(response.status_code, 404)

        ProfilAuteur.objects.create(
            is_author=True,
            user=self.user,
        )

        response = self.client.get('/auteur/admin1234/')
        self.assertEqual(response.status_code, 200)

    def tag_detail(self):
        Tag.objects.create(name='foo')
        response = self.client.get('tag/foo/')
        self.assertEqual(response.status_code, 200)
