# -*- coding: utf-8 -*-

from django.contrib import admin
from bugreport.models import *
from texte.models import *
import bugreport.forms


class RapportErreurAdmin(admin.ModelAdmin):
    list_display = ('id', 'texte', 'nom_rapporteur', 'bouton_effacer')
    list_filter = ['texte']
    list_display_links = ['id', 'texte']
admin.site.register(RapportErreur, RapportErreurAdmin)

