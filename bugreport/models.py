# -*- coding: utf-8 -*-

from django.db import models

from texte.models import *



class RapportErreur(models.Model):
    texte = models.ForeignKey(Texte, on_delete=models.CASCADE)
    page = models.ForeignKey(Page, on_delete=models.CASCADE)
    nom_rapporteur = models.CharField(max_length=30)
    email_rapporteur = models.CharField(
        max_length=30,
        help_text="Cette adresse ne sera pas publiée, elle sera utilisée pour vous tenir au courant de la correction de l'erreur", null=True, blank=True)
    extrait = models.TextField(
        max_length=600,
        help_text='Copier/coller (depuis le <a href="#texte">texte ci-dessous</a>) la phrase ou le morceau de phrase concerné.'
        )
    libelle = models.TextField("En quoi consiste l'erreur ?",max_length=600)

    def __str__(self):
        return '#%s' % self.id

    def get_absolute_url(self):
        return "erreur/#%i" % self.id


    def bouton_effacer(self):
        """Permet d'afficher dans le détail de la page d'admin un lien
        pour effacer le rapport.
        """
        return mark_safe('<a href="%s/delete/">Effacer</a>' % self.id)
    bouton_effacer.short_description='Effacer l\'erreur'
