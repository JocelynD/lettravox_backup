from bugreport.models import *
import django.forms as forms

class AjoutErreur(forms.ModelForm):
    class Meta:
        model = RapportErreur
        exclude = ('texte','page')

#     def as_div(self):
#         """Same as standard as_p but put help text but surround
#         help_text between <span class=help_text>.
#         """
#         return self._html_output(u'<div>%(label)s %(field)s<br /><p class="help">%(help_text)s</p></div>', u'%s', '</p>', u' %s', True)



class SelectionPage(forms.Form):
    # FIXME: should validate page number.
    page = forms.IntegerField()
