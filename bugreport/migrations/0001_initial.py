# -*- coding: utf-8 -*-


from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('texte', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='RapportErreur',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nom_rapporteur', models.CharField(max_length=30)),
                ('email_rapporteur', models.CharField(help_text=b"Cette adresse ne sera pas publi\xc3\xa9e, elle sera utilis\xc3\xa9e pour vous tenir au courant de la correction de l'erreur", max_length=30, null=True, blank=True)),
                ('extrait', models.TextField(help_text=b'Copier/coller (depuis le <a href="#texte">texte ci-dessous</a>) la phrase ou le morceau de phrase concern\xc3\xa9.', max_length=600)),
                ('libelle', models.TextField(max_length=600, verbose_name=b"En quoi consiste l'erreur ?")),
                ('page', models.ForeignKey(to='texte.Page', on_delete=models.CASCADE)),
                ('texte', models.ForeignKey(to='texte.Texte', on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
