# -*- coding: utf-8 -*-

from django.shortcuts import get_object_or_404, render
import django.http
from django.core.mail import send_mail

from captchaproxy.decorators import captcha_protected

import bugreport.forms
from bugreport.models import *
import texte.models
from texte.views import generic_edit

# Create your views here.
def signaler_erreur_texte(requete, slug):
    t = get_object_or_404(texte.models.Texte, slug=slug)
 
    if not requete.POST:
        if t.is_unipage():
            page = get_object_or_404(texte.models.Page, html_repr__parent=t, no=1)
            return django.http.HttpResponseRedirect("%ssignaler-erreur/%i/" % (t.get_absolute_url(), page.no))

        else:
            f = bugreport.forms.SelectionPage()
            cont = {'form':f, 'object':t}
            return render(requete, 'bugreport/choisir_page.html',cont)
    
    else:
        f = bugreport.forms.SelectionPage(requete.POST)
        
        if f.is_valid():
            page = get_object_or_404(texte.models.Page, html_repr__parent=t, no=f.cleaned_data['page'])
            return django.http.HttpResponseRedirect("%ssignaler-erreur/%i/" % (t.get_absolute_url(), page.no))
        
        else:
            cont= {'form':f, 'object':t}
            return render(requete, 'bugreport/choisir_page.html', cont)

@captcha_protected
def signaler_erreur_page(requete, slug, no_page):
    t = get_object_or_404(texte.models.Texte, slug=slug)
    p = texte.models.Page.objects.get(no=no_page, html_repr__parent=t)
    
    if requete.POST:
        f = bugreport.forms.AjoutErreur(requete.POST)
        if f.is_valid():
            rapport = f.save(commit=False)
            rapport.texte = t
            rapport.page = p
            rapport.save()

            # Notification mail
            admin_url = "%s/admin/bugreport/rapporterreur/%i/" % (settings.SITE_URL, rapport.id)
            delete_url = "%s/delete/" % admin_url

            corps = "%s a rapporté une erreur sur %s :\n\nDans l'extrait:\n\n%s\n\nL'erreur :\n\n%s\n\n" %\
                (rapport.nom_rapporteur, t, rapport.extrait, rapport.libelle) +\
                "Corriger l'erreur : %s\n" % admin_url +\
                "Rapport Indésirable ? le supprimer : %s\n" % delete_url +\
                "Prendre contact avec le rapporteur : %s" % rapport.email_rapporteur

            send_mail('[%s] Nouveau commentaire sur %s' % (settings.SITE_NAME, t),
                      corps, settings.DEFAULT_FROM_EMAIL,
                      [t.auteur.email], fail_silently=False)


            return django.http.HttpResponseRedirect('merci/')
            
    else:
        f = bugreport.forms.AjoutErreur()
        
    cont = {'form': f,
            'page': p,
            'object': t
    }
    return render(requete, 'bugreport/rapporterreur_add.html', cont)


def merci(requete, slug, no_page):
    t = get_object_or_404(texte.models.Texte, slug=slug)
    page = texte.models.Page.objects.get(no=no_page, html_repr__parent=t)
    
    cont = {'page':page, 'object':t}
    return render(requete, 'bugreport/merci.html',cont)


def fix_edit(requete, no_bug, **args):
    b = get_object_or_404(RapportErreur,pk=no_bug)
    had_to_compile , fw_valeurs = generic_edit(requete, b.texte)
    fw_valeurs['bug'] = b
    fw_valeurs['referer'] = requete.META.get('HTTP_REFERER')
    if had_to_compile:
        return fw_valeurs
    else:
        return render(requete, 'admin/bugreport/corriger_erreur.html', fw_valeurs)
