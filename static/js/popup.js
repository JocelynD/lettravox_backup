function open_ext_link()
{
    var liens = document.getElementsByTagName('a');
    // On récupère tous les liens (<a>) du document dans une variable (un array), ici liens.
    // Une boucle qui parcourt le tableau (array) liens du début à la fin.
    for (var i = 0 ; i < liens.length ; ++i)  {
	// Si les liens ont un nom de class égal à lien_ext, alors on agit.
	if (liens[i].className == 'lien_ext')  {
	    liens[i].title = 'S\'ouvre dans une nouvelle fenêtre';
	    // Au clique de la souris.
	    liens[i].onclick = function()  {
		window.open(this.href);
		return false; // On ouvre une nouvelle page ayant pour URL le href du lien cliqué et on inhibe le lien réel.
	    };
	}
    }
}
window.onload = open_ext_link;
// Au chargement de la page, on appelle la fonction.
