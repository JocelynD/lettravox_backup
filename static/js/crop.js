squareHeight = 0;
squareWidth = 0;

/**
 * Displays a fullscreen loading image until the page is closed.
 * msg is displayed bellow the loading icon.
 */
function ajaxLoad(msg) {
    var load_mask = document.createElement('div'),
	loading = document.createElement('div'),
	p_msg = document.createElement('p');

    load_mask.id = 'loading_mask';
    loading.innerHTML = '<img src ="/static/img/loading.gif" />';
    p_msg.innerHTML = msg;

    load_mask.appendChild(loading);
    load_mask.appendChild(p_msg);
    $$('body')[0].appendChild(load_mask);
}

toggleCrop = function() {return true};

function editDOM() {
    var toAdd = 
	'<div id="mask_left" class="mask"></div><div id="mask_top" class="mask"></div>'
	+'<div id="mask_right" class="mask"></div><div id="mask_bottom" class="mask"></div>';
    var masks = document.createElement("div");
    masks.innerHTML = toAdd;
    $$('fieldset')[0].appendChild(masks);
}

function hideUselessElements() {
    $$('.field-square_x_pos')[0].hide();
    $$('.field-square_y_pos')[0].hide();
    $$('.field-square_size')[0].hide();
    $$('.field-square .file-upload')[0].hide();
}


function startObserveMouse(e) {
    Event.observe($("illustration"), "mousemove", squareCropper);
    Event.observe($("mask_top"), "mousemove", squareCropper);
    Event.observe($("mask_right"), "mousemove", squareCropper);
    Event.observe($("mask_bottom"), "mousemove", squareCropper);
    Event.observe($("mask_left"), "mousemove", squareCropper);
}

function stopObserveMouse() {
    Event.stopObserving($("illustration"), "mousemove", squareCropper);
    Event.stopObserving($("mask_top"), "mousemove", squareCropper);
    Event.stopObserving($("mask_right"), "mousemove", squareCropper);
    Event.stopObserving($("mask_bottom"), "mousemove", squareCropper);
    Event.stopObserving($("mask_left"), "mousemove", squareCropper);
}


function squareCropper(e) {
    stopObserveMouse();
    // the element that triggered the event  
    var element = $("illustration");  
    // gets the mouse position  
    var mouseX = Event.pointerX(e) ,
	mouseY = Event.pointerY(e);

    var maxX = element.offsetLeft + element.width - window.squareWidth/2,
	minX = element.offsetLeft + window.squareWidth/2,
	maxY = element.offsetTop + element.height - window.squareHeight/2,
	minY = element.offsetTop + window.squareHeight/2;

    //On ne veut pas que notre carré sorte de l'image.
    if      (mouseX < minX) mouseX = minX;
    else if (mouseX > maxX) mouseX = maxX;

    if      (mouseY < minY) mouseY = minY;
    else if (mouseY > maxY) mouseY = maxY;

    moveMask(element, mouseX, mouseY);
    // stop default behaviour and event propagation  
    Event.stop(e);
    startObserveMouse();
}


function initMasks(image, square_x, square_y) {
    var squareLeft = image.offsetLeft + square_x,
	squareRight = squareLeft + window.squareWidth,
	squareTop = image.offsetTop + square_y,
	squareBottom = squareTop + window.squareHeight;

    $("mask_left").setStyle({
	    width : 0,//quare_x+'px',
		height : image.height+'px', // FIXED
		left : image.offsetLeft+'px',// FIXED
		top : image.offsetTop+'px', //FIXED
		});

    $("mask_right").setStyle({
	    width : 0,//image.width - square_x - window.squareWidth+'px', //MOVES
		height : image.height+'px',
		left : squareRight+'px',//MOVES
		top : image.offsetTop+'px'
		});

    $("mask_bottom").setStyle({
	    width : window.squareWidth+'px', 
		height : 0,//image.height - square_y - window.squareHeight +'px', 
		left : squareLeft+'px', 
		top : squareBottom+'px'
		});

    $("mask_top").setStyle({
	    width : window.squareWidth+'px',
		height : 0,//square_y+'px',
		left : squareLeft+'px',
		top : image.offsetTop+'px' //FIXED
		});
}

function moveMask(image, mouse_x, mouse_y) {
    var squareLeft = mouse_x - window.squareWidth/2,
	squareRight = squareLeft + window.squareWidth,
	squareTop = mouse_y - window.squareHeight/2,
	squareBottom = squareTop + window.squareHeight,
	square_x = squareLeft - image.offsetLeft,
	square_y = squareTop - image.offsetTop;
    
    $("mask_left").setStyle({
	    width : square_x +'px',
		});


    $("mask_right").setStyle({
	    width : image.width - square_x - window.squareWidth+'px', //MOVES
		left : squareRight+'px',//MOVES
		});

    $("mask_bottom").setStyle({
	    width : window.squareWidth+'px', 
		height : image.height - square_y - window.squareHeight +'px', 
		left : squareLeft+'px', 
		top : squareBottom+'px'
		});

    $("mask_top").setStyle({
	    width : window.squareWidth+'px',
		height : square_y+'px',
		left : squareLeft+'px',
		});
}


function stopCropping() {
    new Event.observe(image, "click", function() {
	    startObserveMouse();
	    new Event.observe(image, "click", function() {
		    stopObserveMouse();
	    });
    });

}


function instrumentImage(el) {
    var image = el;
    var minSide = image.width;
    if (image.height < minSide)
	minSide = image.height;
    window.squareWidth = 0.33*minSide;
    window.squareHeight = 0.33*minSide;
    initMasks(el, 100, 100);

    // On démarre le cropping à partir du moment où l'on clique sur le carré

    new Event.observe($('square'), "click", function(e) {
	squareCropper(e);
	startObserveMouse();
	toggleCrop = function() {return false;}
    });

    new Event.observe(image, "click", function(e) {
	if (toggleCrop()) {
	    squareCropper(e);
	    startObserveMouse();
	    toggleCrop = function() {return false;}
	} else {
	    stopObserveMouse();
	    ajaxLoad('Génération de l\'imagette...');

	    // On sauvegarde les paramètres
	    // 			$('x_crop').innerHTML = $('mask_left').style.width;
	    // 			$('y_crop').innerHTML = $('mask_top').style.height;
	    // 			$('crop_width').innerHTML = window.squareWidth;

	    var thumb_ratio =
		parseInt($('full_illustration_width').value)/image.width;

	    $('id_square_x_pos').value = parseInt(
		parseInt($('mask_left').style.width.split('px')[0])
		    *thumb_ratio);
	    $('id_square_y_pos').value = parseInt(
		parseInt($('mask_top').style.height.split('px')[0])
		    *thumb_ratio);
	    $('id_square_size').value = parseInt(
		parseInt(window.squareWidth)*thumb_ratio);

	    toggleCrop = function() {return true;}

	    let saveAndContinueBtns = $$('#illustration_form input[type=submit][name=_continue]');
	    if (saveAndContinueBtns.length > 0) {
		saveAndContinueBtns[0].click()
	    } else {
		alert("Recadrage de l'aperçu pris en compte");
		$$('#illustration_form input[type=submit]')[0].click();
	    }
	}
    });
}

function load() {
    let imageEl = $("illustration")
    editDOM();
    hideUselessElements();
    if (imageEl) {
	/* We have to wait for the image to be ready… but we can miss the load
	 * event as it can load before our script. So try to handle all the
	 * cases.
	 */
	if (imageEl.complete) {
	    instrumentImage(imageEl);
	} else {
	    imageEl.observe('load', function() {
		instrumentImage(imageEl);
	    });
	}
    } else { //Cas d'une nouvelle illustration
	$("id_square").parentNode.parentNode.hide();
    }
}

new Event.observe(window, "dom:loaded", load);
