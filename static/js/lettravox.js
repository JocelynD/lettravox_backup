
var bound_form = false;

function aller_a(choix){
    window.location=choix.options[choix.selectedIndex].value;
}

function comment_form(texte_slug) {
    if (!$('ajax_commentform')) {
	new Ajax.Updater(
	   'combut',
	   '/textes/' + texte_slug  + '/ajax/commenter/',
	   {method: 'get', insertion: Insertion.After}
	);
    }
}

function post_comment(texte_slug) {
    new Ajax.Updater(
        'ajax_commentform',
	'/textes/' + texte_slug  + '/ajax/commenter/',
	{method: 'post', parameters: $('ajax_commentform').serialize(true)}
    );
}
var originalComments, lightComments;

function max(a, b) {
	return (a > b) ? a : b;
}

function bottomOffset(e) {
	return e.cumulativeOffset()[1] + e.getHeight();
}

function isElementInViewport (el) {

    //special bonus for those using jQuery
    if (typeof jQuery === "function" && el instanceof jQuery) {
        el = el[0];
    }

    var rect = el.getBoundingClientRect();

    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
        rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
    );
}

//var t;
function short_comment_show() {
	var coms = $$('#comlist .commentaire');
	//console.log('oms:', coms);
	var i = 1;
	var overflow = false;
	var maxColPos = max(bottomOffset($('content')),
					bottomOffset($('sidebar')));
	//console.log("mmaaaaaaaaax", maxColPos);

	while ((i < coms.length) && (! overflow)) {
		com = coms[i];
		overflow = (bottomOffset(com) > maxColPos);
		//console.log("For ", com, " at ", i, " overflow is ", overflow);
		i++;
	}
	var last;
	if (i > 2) {
		i--;
	}
	//console.log("i +1 is ", i +1, " length is ", coms.length)
	if (coms.length >= (i+1) ) {
		last = coms[i-1];
		// Delete the next sibblings.

		var next_coms = last.nextSiblings();
		for (var j = 1 ; j < next_coms.length; j++) {
			//console.log("DEL " + next_coms[j].innerHTML);
			next_coms[j].remove();
		}

		// last.insert({bottom: date_clone});
		// Bouton "+" avec le bon label selon ce que contient la suite
		var more_but = '<div id="suite_coms">'
			+'<a href="#" onclick="full_comments_show(); return false">▼ '
			+(coms.length - i)  + ' autres commentaires ▼</a></div>';

		last.next('div').insert({bottom: more_but});
	}
}

function full_comments_show() {
	var last_com_idx = 	$$('#comlist .commentaire').length - 1;
    reducedComments = $('comlist').innerHTML;
	$('comlist').innerHTML = originalComments;
	//$$('#comlist .commentaire')[last_com_idx].scrollTo();

	var less_but = '<div id="suite_coms">'
		+'<a href="#" onclick="short_comment_show(); return false">▲ '
		+ ' Cacher ▲</a></div>';
	$('comlist').insert({'bottom': less_but});
}

/*
Event.observe(window, 'load', function() {
    originalComments = $('comlist').innerHTML;
	short_comment_show();
});
//*/

// Précharge les images, pour éviter le clignotement sur le hover
function preload(url) {
    var img = new Image();
    img.src=url;
}

var car;

function isArticle(e) {
	return $(e).match('.article');
}

function reborder() {
	$$('.bordered')[0].removeClassName('bordered')
	right_candidate = $$('.article:not(.hidden)')[0].addClassName('bordered')
}

Event.observe(window, 'load', function() {
	preload("/static/img/right-slide-hover.png");
	preload("/static/img/left-slide-hover.png");

    var right_candidate;
    var left_candidate;

    if ($('right-slider')) {
	    $('right-slider').observe('click', function(e) {
		    right_candidate = $$('.article:not(.hidden)').last().next();
		    if (isArticle(right_candidate)) {
			    right_candidate.removeClassName('hidden');
			    $$('.article:not(.hidden)').first().addClassName('hidden');
			    $('left-slider').show();

			    // On a atteint le bout.
			    if (! right_candidate.next().hasClassName('article')) {
				    $('right-slider').hide();
			    }
		    }
		    e.stop();
	    });

	    $('left-slider').observe('click', function(e) {
		    left_candidate = $$('.article:not(.hidden)').first().previous();
		    if (isArticle(left_candidate)) {
			    left_candidate.removeClassName('hidden');
			    $$('.article:not(.hidden)').last().addClassName('hidden');

			    $('right-slider').show();

			    // On a atteint le bout.
			    if (! left_candidate.previous().hasClassName('article')) {
				    $('left-slider').hide();
			    }
		    }
            e.stop();
	    });
    }

    // RSS/email subscription links
    $$('.abo-links').forEach(function(aboBox) {
        aboBox = $(aboBox);
        ['rss', 'email'].forEach(function(linkType) {
            var link = aboBox.down('a.'+linkType);
                var formContainer = aboBox.down('div.'+linkType);
            link.observe('click', function(event) {
                $$('.abo').forEach(Element.hide);
                formContainer.show();
                if (!isElementInViewport(formContainer)) {
                    formContainer.scrollIntoView(false);
                }
                event.stop();
            });
        });
    });
    $(document.body).observe('click', function(event) {
        if (! event.findElement('.abo')) {
            $$('div.abo').forEach(Element.hide);
        }
    });
});