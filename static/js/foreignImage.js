function add_foreign_edit(field_name, app_name, model_name) {
    var field_parent = $$('.'+ field_name +' div')[0],
	select = $$('.'+ field_name + ' div select')[0],
	edit_button = document.createElement('a');
    if (select.value)
	edit_button.href = '/admin/'+ app_name +'/'+ model_name +'/' + select.value;
    else
	edit_button.hide();

    edit_button.innerHTML = 'Modifier cette image ou la zone d\'aperçu';
    field_parent.appendChild(edit_button);

    Event.observe(select,'change', function(e) {
        if (select.value) {
	    edit_button.href = '/admin/'+ app_name +'/'+ model_name +'/' + select.value;
	    edit_button.show();
	} else {
	    edit_button.hide();
	}
    });

    // opens a popup
    Event.observe(edit_button, 'click', function(e) {
      window.open(edit_button.href);
      Event.stop(e);
    });

}

function load_foreign_edit() {
    add_foreign_edit('illustration', 'texte', 'illustration');
}

new Event.observe(window, "dom:loaded", load_foreign_edit);