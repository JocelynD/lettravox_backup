/*
 * CodePress regular expressions for Latex syntax highlighting
 * Written by Jocelyn Delalande
 * Inspired by emacs latex-mode behavior.
 */



// LaTeX
Language.syntax = [
	{ input : /\%(.*?)(<br>|<\/P>)/g, output : '<i>%$1</i>$2' }, // % comment
// Some special commands
        { input : /\\(newpage)/g,output : '<span>\\$1</span>'},
        { input : /\\footnote\{(.*?)\}/g,output : '<blink>\\footnote<\/blink><blink>{$1}<\/blink>'},
	{ input : /(«|\"|``)(.*?)(»|\"|'')/g,output : '<cite>$1$2$3<\/cite>'},
// Short form
	{ input : /\{(\\\w*)(\s+.*?)\}/g,output : '<a>{$1$2}<\/a>'},
// Bold
	{ input : /\{\\bf(\s.*?)\}/g,output : '{\\bf<b>$1<\/b>}'},
        { input : /\\textbf\{(.*?)\}/g,output : '<s>\\textbf<\/s><b><font>{$1}<\/font></b>'},
//Italic
	{ input : /\{\\it(\s.*?)\}/g,output : '{\\it<i>$1<\/i>}'},
        { input : /\\textit\{(.*?)\}/g,output : '\\textit<i><font>{$1}<\/font><\/i>'},
// Sectionning
// \chapter or \section
        { input : /\\(chapter|section)(\*?)\{(.*?)\}/g, output : '<s>\\$1$2<\/s><big>{$3}<\/big>'},
// \subsection or \subsubsection
        { input : /\\(subsubsection|subsection)(\*?)\{(.*?)\}/g, output : '<s>\\$1$2<\/s><strong>{$3}<\/strong>'}, 
// \paragraph or \subparagraph
        { input : /\\((sub)?paragraph)(\*?)\{(.*?)\}/g, output : '<s>\\$1$3<\/s><em>{$4}<\/em>'} ,
        { input : /\\(institute|author|title)\{(.*?)\}/g, output : '<s>\\$1<\/s><a>{$2}<\/a>'} ,
// Any command with an arg
        { input : /(\\\w*)(\[.*?\])?((\{.*?\}){1,2})/g,output : '<s>$1<\/s><ins>$2<\/ins><u>$3<\/u>'},
// Any command without arg
        { input : /(\\\w*?)(<br|<\/P)/g,output : '<small>$1<\/small>$2'},
]

Language.snippets = []
 
Language.complete = [
	{ input : '"', output : '"$0"' },
	{ input : '(', output : '\($0\)' },
	{ input : '{', output : '\{$0\}' },
	{ input : '[', output : '\[$0\]' },
	{ input : '«', output : '« $0 »' },
        

]

Language.shortcuts = []
