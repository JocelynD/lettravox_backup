#!/usr/bin/env python
from django.core.servers.fastcgi import runfastcgi

# Switch to the directory of your project. (Optional.)
# os.chdir("/home/jdoe/django-projects/blog"

runfastcgi(method="threaded", daemonize="false")
