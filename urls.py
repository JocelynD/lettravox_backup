# -*- coding: utf-8 -*-
from django.contrib.admin.views.main import *
from django.conf import settings
from django.conf.urls import include, url
from django.contrib import auth, admin
from django.views.generic import RedirectView, TemplateView
from django.views.static import serve

from articles.views import (
    ArticleDetailView, EventListView, PublicationListView)
from bugreport.views import (
    fix_edit,
    signaler_erreur_page,
    merci,
    signaler_erreur_texte,
)

from texte.views import (
    AuthorListView, AuthorDetailView, LinkListView,
    TagDetailView, TextListView, compiler, source_edit, commenter,
    ajax_commenter, page_en_ligne, telecharger)

admin.autodiscover()

urlpatterns = [
    url(r'^admin/texte/texte//(?P<id>.+)/compiler/$', compiler, name='compiler'),
    url(r'^admin/texte/texte/(?P<id>.+)/editeur/$', source_edit, name='source_edit'),
]

urlpatterns += [
    url(r'^admin/bugreport/rapporterreur/(?P<no_bug>\d+)/$', fix_edit),
]

urlpatterns += [
    # Accueil
    url(r'^$', TemplateView.as_view(template_name='accueil.html')),
    url(r'^apropos/$', TemplateView.as_view(template_name='apropos.html')),

# Admin
    url(r'^admin/', admin.site.urls),

# Contenu statique
    url(r'^site_media/(?P<path>.*)$', serve,
        {'document_root': settings.MEDIA_ROOT}),

    # Textes
    url(r'^textes/$', TextListView.as_view()),
    url(r'^textes/(?P<slug>[-\w]+)/lire/$', RedirectView.as_view(
        url='1/', permanent=True)),
    url(r'^textes/(?P<slug>[-\w]+)/$', RedirectView.as_view(
        url='lire/1/', permanent=True)),

    # Auteurs
    url(r'^auteurs/$', AuthorListView.as_view()),
    url(r'^auteur/(?P<slug>\w+)/$', AuthorDetailView.as_view(), name="auteur-detail"),

    # Liens
    url(r'^amis/$', LinkListView.as_view()),

    # Tags, détail
    url(r'^tag/(?P<slug>[-\w]+)/$', TagDetailView.as_view()),
]

# Syndication rss

urlpatterns += [
    url(r'^abonnements/', include('subscriptions.urls')),
]

urlpatterns += [
        url(r'^textes/(?P<slug>[-\w]+)/commenter/(?P<no_page>\d+)/?$', commenter),
        url(r'^textes/(?P<slug>[-\w]+)/ajax/commenter/$', ajax_commenter),
        url(r'^textes/(?P<slug>[-\w]+)/lire/(?P<no_page>\d+)/?$', page_en_ligne),
        url(r'^textes/(?P<slug>[-\w]+)/telecharger/$', telecharger),
]

## Bugreports
urlpatterns += [
        url(r'^textes/(?P<slug>[-\w]+)/signaler-erreur/$',signaler_erreur_texte),
        url(r'^textes/(?P<slug>[-\w]+)/signaler-erreur/(?P<no_page>\d+)/$',signaler_erreur_page),
        url(r'^textes/(?P<slug>[-\w]+)/signaler-erreur/(?P<no_page>\d+)/merci/$',merci)
]

## Captcha from simple-captcha
urlpatterns += [
    url(r'^captcha/', include('captcha.urls')),
]

# Actus

urlpatterns += [
    url(r'^publications-papier/$', PublicationListView.as_view(),
     name='paperback-list'),
    url(r'^actu/(?P<slug>[-\w]+)/?$',
        ArticleDetailView.as_view(), name='article-detail'),
    url(r'^evenements/$', EventListView.as_view(), name='event-list'),
]

## Markdownx

urlpatterns += [
    url(r'^markdownx/', include('markdownx.urls'))
]
#
## Legacy
#
urlpatterns += [
    url(r'^syndication/derniers-textes/$',
     RedirectView.as_view(url='abonnement/rss/texts/', permanent=True)),
]
