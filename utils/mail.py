# -*- coding: utf-8 -*-


from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string


def send_service_mail(subject, to, body_template, context={}):
    send_mail(
        "[{}] {}".format(settings.SITE_NAME, subject),
        render_to_string(body_template, context),
        settings.DEFAULT_FROM_EMAIL,
        [to], fail_silently=False)
