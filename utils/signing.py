from datetime import datetime, timedelta

from django.conf import settings
from django.core import signing
from django.core.signing import BadSignature

class TokenSigner:
    def __init__(self, subject):
        self.subject = subject

    def sign(self, payload):
        return signing.dumps((self.subject, payload), compress=True)

    def unsign(self, data):
        subject, payload = signing.loads(data,
                                         max_age=settings.JWT_DEFAULT_LIFETIME)
        if subject != self.subject:
            raise BadSignature('Wrong signed subject')
        else:
            return payload
