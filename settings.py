# -*- coding: utf-8 -*-
from django.core.management.base import CommandError

from default_settings import *
try:
    from local_settings import *
except ImportError:
    raise CommandError('You should put a local_settings.py, see README.md')
