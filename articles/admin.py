# -*- coding: utf-8 -*-

from django.contrib import admin
from django import forms
from django.db import models
from django.utils.html import strip_tags
from markdownx.admin import MarkdownxModelAdmin
from slughifi import slugify

from articles.models import Article


class ArticleAdminForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = '__all__'

    def clean_accroche(self):
        # Nombres max de caractères sur chaque
        max_l1 = 27
        max_l2 = 30

        t = self.cleaned_data['accroche']

        if t.find('<br />') >= 0:
            l1, l2 = t.split('<br />')
        else:
            l1, l2 = t, ''

        if len(strip_tags(l1)) > max_l1:
            raise forms.ValidationError(
                "Première ligne (avant \"<br />\")  trop longue, "\
              + "dépasse du bandeau.")

        elif len(strip_tags(l2)) > max_l2:
            raise forms.ValidationError(
                "Deuxième ligne (après \"<br />\")  trop longue, "\
              + "dépasse du bandeau.")

        return t

    def clean_titre(self):
        # Prevent bare crash on Model.save() using prior validation
        titre = self.cleaned_data['titre']
        query = Article.objects.filter(slug=slugify(titre))
        if self.instance.pk:
            query = query.exclude(pk=self.instance.pk)
        if query.exists():
            raise forms.ValidationError(f'Ce nom est déjà pris')
        return titre


class ArticleAdmin(MarkdownxModelAdmin):
    form = ArticleAdminForm
    list_display = ('titre','pub_date', 'highlight', 'categorie')
    fieldsets = (
        (None, {'fields': (
            'titre',
            'publication_status',
            'pub_date',
            'categorie',
            'concerne', 'collectif',
        )}),
        ('Méta-données', {'fields':
                          ('chapeau', 'accroche',
                           'highlight',
                           'mini_illustration')}),
        ('Corps de l\'article', {'fields': ('markdown_text',)}),
        ('Options avancées', {'fields': ('texte',), 'classes': ('collapse',)}),
    )

    formfield_overrides = {
        models.ManyToManyField: {'widget': forms.CheckboxSelectMultiple},
    }


    def save_model(self, request, obj, form, change):
        # Si l'objet est nouveau, on définit son auteur
        # automatiquement.
        if not hasattr(obj, 'auteur'):
            obj.auteur = request.user
        obj.save()


admin.site.register(Article, ArticleAdmin)
