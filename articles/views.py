# -*- coding: utf-8 -*-

from django.views.generic import DetailView, ListView

from .models import Article


class AbstractArticleListView(ListView):
    def get_queryset(self):
        return Article.objects\
                      .published()\
                      .filter(categorie=self.category)\
                      .order_by('-pub_date')

    def get_context_data(self, **kwargs):
        context = super(AbstractArticleListView, self)\
                  .get_context_data(**kwargs)
        context['categorie'] = self.category_verbose_name
        return context


class PublicationListView(AbstractArticleListView):
    category = Article.PUBLICATION
    category_verbose_name = 'Publications papier'


class EventListView(AbstractArticleListView):
    category = Article.EVENEMENT
    category_verbose_name = 'Événements'

class ArticleDetailView(DetailView):
    queryset = Article.objects.all()
