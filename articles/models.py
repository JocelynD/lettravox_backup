# -*- coding: utf-8 -*-

from datetime import datetime
import os

from django.db import models
from django.conf import settings
from django.contrib import auth
from django.core.files import File
from django.db.models import Q
from django.utils import timezone
from markdownx.models import MarkdownxField
from markdownx.utils import markdownify
from slughifi import slugify


class ArticleQuerySet(models.QuerySet):
    def published(self):
        return self.filter(
            pub_date__lte=timezone.now(),
            publication_status=Article.PUB_STATE_PUBLISHED,
        )

class Article(models.Model):
    """ Contient un texte publié, sa description, sa source Tex, sa
    représentation html & pdf, ses métadonnées et ses commentaires
    """
    PUBLICATION = 'P'
    EVENEMENT = 'E'
    CATEGORIES = ((PUBLICATION,"Publications papier"), (EVENEMENT, "Événements"))

    PUB_STATE_PUBLISHED='published'
    PUB_STATE_DRAFT='draft'
    PUB_STATES = (
        (PUB_STATE_PUBLISHED, 'Publié'),
        (PUB_STATE_DRAFT, 'Brouillon'),
    )

    titre = models.CharField(max_length=200)
    auteur = models.ForeignKey(auth.models.User, blank=True,
                               on_delete=models.CASCADE)
    slug = models.SlugField(unique=True)
    publication_status = models.CharField(
        'État de publication',
        help_text='Les textes brouillon ne sont pas listés publiquement sur le site',
        choices=PUB_STATES, default=PUB_STATE_PUBLISHED, max_length=20,
    )
    pub_date = models.DateTimeField(
        'Date de publication',
        help_text='Mettre une date dans le futur pour publication planifiée automatique',
        default=datetime.now)
    highlight = models.BooleanField('Mis en valeur dans le ruban',default=False)
    chapeau = models.TextField("Chapeau", max_length=1500, blank=True)
    categorie = models.CharField(choices=CATEGORIES,blank=True, max_length=1,null=True)
    concerne = models.ManyToManyField(
        auth.models.User, blank=True, related_name="intervient_dans",
        help_text="ex: pour une parution papier, les auteur-e-s")
    collectif = models.BooleanField('Œuvre collective', default=False)
    mini_illustration = models.ImageField(
        upload_to="news-images",
        blank=True,
        null=True)
    accroche = models.TextField("Texte d'accroche apparaîssant sur le bandeau",
                                max_length=500, blank=True)
    texte = models.TextField("Texte de l'article, en HTML", max_length=10000, blank=True)

    markdown_text = MarkdownxField("Texte de l'article, en MarkDown", blank=True, null=True)

    mini_illustration.help_text = "Cette image apparaîtra en petit sur la page"\
        +" d'accueil, pas dans le cœur de l'actu"
    texte.help_text = """Ce texte peut-être formatté librement en HTML.
À n'utiliser que si vous souhaitez avoir un contrôle plus fin sur la mise en forme ;
auquel cas, videz le champ « texte de l'article en markdown »"""
    chapeau.help_text = "Ce chapeau apparaîtra sur la page d'accueil. Il ne"\
        + " peut pas être formatté en HTML"
    accroche.help_text = "L'accroche apparaît dans le bandeau en"\
        +" haut à droite du site. Du texte entre les balises &lt;span&gt;&lt;/span&gt; "\
        +"apparaîtra d'une couleur différente du reste du texte. la balise &lt;br /&gt;"\
        +" sert à passer à la deuxième ligne du bandeau."

    class Meta:
        ordering = ['-id']

    objects = ArticleQuerySet.as_manager()

    def __str__(self):
        return self.titre

    def save(self, *args, **kwargs):
        if not self.mini_illustration:
            default_illustration_path = "news-images/article_defaut.png"

            self.mini_illustration = File(open(
                    os.path.join(settings.MEDIA_ROOT, default_illustration_path), 'rb'))

            # converts to relative name
            self.mini_illustration.name = default_illustration_path

        self.slug = slugify(self.titre)

        if self.markdown_text:
            self.texte = markdownify(self.markdown_text)
        super(Article, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return "/actu/%s/" % self.slug

    def is_publication(self):
        return self.categorie == 'P'

    def is_collective(self):
        return (self.concerne.count() > 1) or self.collectif


# Équipement pour les Auteurs
auth.models.User.ouvrages_papier = lambda s:\
    Article.objects.published()\
                   .annotate(num_auteurs=models.Count('concerne'))\
                   .filter(concerne=s, categorie='P')\
                   .order_by('-pub_date')


auth.models.User.ouvrages_papier_auteur = lambda s:\
    s.ouvrages_papier().filter(num_auteurs=1).filter(collectif=False)

auth.models.User.ouvrages_papier_co_auteur = lambda s:\
    s.ouvrages_papier().filter(Q(num_auteurs__gt=1) | Q(collectif=True))
