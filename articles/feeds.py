# -*- coding: utf-8 -*-
from django.contrib.syndication.views import Feed
from django.conf import settings

from articles.models import Article

class EvenementsRSS(Feed):
    title = "%s - Derniers événements" % settings.SITE_NAME
    short_title = 'Derniers événements'
    prefix = 'Événement'
    link = "/evenements/"
    description = "Derniers événements annoncés sur %s" % settings.SITE_NAME
    categories = ['Événement', 'actu']

    def items(self):
        return Article.objects.published()\
                              .filter(categorie=Article.EVENEMENT)\
                              .order_by('-pub_date')[:10]

    def item_title(self, item):
        return item.titre

    def item_description(self, item):
        return item.chapeau

    def author_name(self, item):
        if item:
            return item.auteur


class PublicationsRSS(Feed):
    title = "%s - Dernières publications papier" % settings.SITE_NAME
    short_title = "Dernières publications papier"
    prefix = "Publication"
    link = "/publications-papier/"
    description = "Dernières parutions de publications papier annoncées sur %s" % settings.SITE_NAME
    categories = ['parution papier', 'actu']

    def items(self):
        return Article.objects.published()\
                              .filter(categorie=Article.PUBLICATION)\
                              .order_by('-pub_date')[:10]

    def item_title(self, item):
        return item.titre

    def item_description(self, item):
        return item.chapeau

    def author_name(self, item):
        if item:
            return item.auteur
