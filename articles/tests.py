# -*- coding: utf-8 -*-
import datetime

from django.test import TestCase
from django.contrib.auth.models import User
from django.urls import reverse

from articles.models import Article

now = datetime.datetime.now()


class ArticlesTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create_superuser(
            username='admin',
            email='admin@example.com',
            password='password')


class AdminTest(ArticlesTestCase):
    def setUp(self):
        super(AdminTest, self).setUp()
        self.assertTrue(
            self.client.login(username='admin', password='password'))

    def test_add_article(self):
        response = self.client.post('/admin/articles/article/add/', {
            'pub_date_0': '2015-11-01',
            'pub_date_1': '23:12',
            'publication_status': 'published',
            'titre': 'TITRE ééàà',
            'categorie': Article.PUBLICATION,
            'chapeau': 'CHAPO chapô',
            'accroche': 'ACCROCHE ééô',
            'texte': 'TEXTE éàô…'
        })
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Article.objects.count(), 1)

    def test_add_duplicate_title_article(self):
        # bug #91
        article = Article.objects.create(
            titre='a', texte='', auteur=self.user)
        response = self.client.post('/admin/articles/article/add/', {
            'pub_date_0': '2015-11-01',
            'pub_date_1': '23:12',
            'publication_status': 'published',
            'titre': 'a',
            'categorie': Article.PUBLICATION,
            'chapeau': '',
            'accroche': '',
            'texte': ''
        })
        self.assertEqual(response.status_code, 200)
        assert 'Ce nom est déjà pris' in response.content.decode()



class PublicTest(ArticlesTestCase):
    def request_article_detail(self, article):
        return self.client.get(reverse('article-detail', args=[article.slug]))

    def test_view_publication_without_author(self):
        article = Article.objects.create(
            titre='éé', texte='héhé',
            categorie=Article.PUBLICATION, auteur=self.user)
        article_url = reverse('article-detail', kwargs={'slug': article.slug})
        response = self.client.get(article_url)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'inconnu', response.content)

    def test_publication_status_default(self):
        article = Article.objects.create(
            titre='hehe4678', texte='a',
            categorie=Article.EVENEMENT, auteur=self.user,
        )
        # Listed
        response = self.client.get(reverse('event-list'))
        self.assertIn(b'hehe4678', response.content)
        # Detail
        response = self.request_article_detail(article)
        self.assertEqual(response.status_code, 200)

    def test_publication_status_draft_status(self):
        article = Article.objects.create(
            titre='hehe4678', texte='a',
            publication_status=Article.PUB_STATE_DRAFT,
            categorie=Article.EVENEMENT, auteur=self.user,
        )
        # Not Listed
        response = self.client.get(reverse('event-list'))
        self.assertNotIn(b'hehe4678', response.content)
        # Accessible (for whom has the URL)
        response = self.request_article_detail(article)
        self.assertEqual(response.status_code, 200)

    def test_publication_status_postponed(self):
        article = Article.objects.create(
            titre='hehe4678', texte='a',
            pub_date=datetime.datetime(2042, 2, 2),
            categorie=Article.EVENEMENT, auteur=self.user,
        )
        # Not Listed
        response = self.client.get(reverse('event-list'))
        self.assertNotIn(b'hehe4678', response.content)
        # Detail accessible (for whom has the URL)
        response = self.request_article_detail(article)
        self.assertEqual(response.status_code, 200)

    def test_view_articles_lists(self):
        events_url = reverse('event-list')
        paperbacks_url = reverse('paperback-list')

        self.assertEqual(self.client.get(events_url).status_code, 200)
        self.assertEqual(self.client.get(paperbacks_url).status_code, 200)

        article = Article.objects.create(
            titre='éé', texte='héhé',
            categorie=Article.PUBLICATION, auteur=self.user)

        self.assertEqual(self.client.get(events_url).status_code, 200)
        self.assertEqual(self.client.get(paperbacks_url).status_code, 200)

        article.categorie = Article.EVENEMENT
        article.save()

        self.assertEqual(self.client.get(events_url).status_code, 200)
        self.assertEqual(self.client.get(paperbacks_url).status_code, 200)
