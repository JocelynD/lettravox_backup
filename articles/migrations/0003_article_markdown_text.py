# -*- coding: utf-8 -*-


from django.db import migrations, models
import markdownx.models


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0002_auto_20160103_2314'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='markdown_text',
            field=markdownx.models.MarkdownxField(null=True, blank=True),
        ),
    ]
