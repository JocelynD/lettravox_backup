# -*- coding: utf-8 -*-


from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='concerne',
            field=models.ManyToManyField(help_text=b'ex: pour une parution papier, les auteur-e-s', related_name='intervient_dans', to=settings.AUTH_USER_MODEL, blank=True),
        ),
    ]
