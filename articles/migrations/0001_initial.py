# -*- coding: utf-8 -*-


from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titre', models.CharField(max_length=200)),
                ('slug', models.SlugField()),
                ('pub_date', models.DateTimeField(default=datetime.datetime.now, verbose_name='Date de publication')),
                ('highlight', models.BooleanField(default=False, verbose_name='Mis en valeur dans le ruban')),
                ('chapeau', models.TextField(help_text="Ce chapeau appara\xc3\xaetra sur la page d'accueil. Il ne peut pas \xc3\xaatre formatt\xc3\xa9 en HTML", max_length=1500, verbose_name='Chapeau', blank=True)),
                ('categorie', models.CharField(blank=True, max_length=1, null=True, choices=[('P', 'Publications papier'), ('E', '\xc3\x89v\xc3\xa9nements')])),
                ('collectif', models.BooleanField(default=False, verbose_name='\xc5\x92uvre collective')),
                ('mini_illustration', models.ImageField(help_text="Cette image appara\xc3\xaetra en petit sur la page d'accueil, pas dans le c\xc5\x93ur de l'actu", null=True, upload_to='news-images', blank=True)),
                ('accroche', models.TextField(help_text="L'accroche appara\xc3\xaet dans le bandeau en haut \xc3\xa0 droite du site. Du texte entre les balises &lt;span&gt;&lt;/span&gt; appara\xc3\xaetra d'une couleur diff\xc3\xa9rente du reste du texte. la balise &lt;br /&gt; sert \xc3\xa0 passer \xc3\xa0 la deuxi\xc3\xa8me ligne du bandeau.", max_length=500, verbose_name="Texte d'accroche appara\xc3\xaessant sur le bandeau", blank=True)),
                ('texte', models.TextField(help_text='Ce texte peut-\xc3\xaatre formatt\xc3\xa9 librement en HTML. Pour des exemples, voir les autres articles. Notez que les images inclues (balise &lt;img&gt;) doivent-\xc3\xaatre d\xc3\xa9j\xc3\xa0 en ligne quelque-part, sur hostile au autre.', max_length=10000, verbose_name="Texte de l'article", blank=True)),
                ('auteur', models.ForeignKey(to=settings.AUTH_USER_MODEL, blank=True, on_delete=models.CASCADE)),
                ('concerne', models.ManyToManyField(help_text='ex: pour une parution papier, les auteur-e-s', related_name='intervient_dans', null=True, to=settings.AUTH_USER_MODEL, blank=True)),
            ],
            options={
                'ordering': ['-id'],
            },
            bases=(models.Model,),
        ),
    ]
